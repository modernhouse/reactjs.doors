import {applyMiddleware, combineReducers, compose, createStore, Store} from "redux";
import {combineEpics, createEpicMiddleware} from "redux-observable";

import {autoRehydrate, persistStore} from "redux-persist";
import * as LocalForage from "localforage";

import Reducer1 from "./state1/Reducer";
import Reducer2 from "./state2/Reducer";
import Epic1 from "./state1/Epic";
import Epic2 from "./state2/Epic";


const createAppStore = (): Store<any> => {
    const reducer = combineReducers({state1: Reducer1, state2: Reducer2});
    const store = createStore(reducer, compose(applyMiddleware(createEpicMiddleware(combineEpics(Epic1,Epic2))), autoRehydrate()));
    persistStore(store, LocalForage);

    return store;
};


export default createAppStore
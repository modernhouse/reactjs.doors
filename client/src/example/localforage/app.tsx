import * as React from "react";
import * as ReactDOM from "react-dom";
import injectTapEventPlugin from "react-tap-event-plugin";

import createAppStore from "./AppStore";

import {Provider} from "react-redux";

import Container1 from "./state1/Container";
import Container2 from "./state2/Container";

import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";




injectTapEventPlugin();

const store = createAppStore();

const render = () => ReactDOM.render(
    <Provider store={store}>
        <MuiThemeProvider>
            <div>
                <Container1/>
                <Container2/>
            </div>
        </MuiThemeProvider>
    </Provider>,
    document.getElementById("root")
);

render();

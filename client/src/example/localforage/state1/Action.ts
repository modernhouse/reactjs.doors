const _request = (action:string):string => {
    return `${action}/request`;
};

export const Action1:string = "state1/action1";
export const Action2:string = "state1/action2";
export const Action3:string = "state1/action3";

export const Action1Request:string = _request(Action1);
export const Action2Request:string = _request(Action2);
export const Action3Request:string = _request(Action3);


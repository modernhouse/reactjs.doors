import * as React from "react";
import {connect} from "react-redux";
import RaisedButton from "material-ui/RaisedButton";
import Chip from 'material-ui/Chip';
import {Action1Request, Action2Request, Action3Request} from "./Action";
import {State} from "./Reducer";


interface Props {
    state: State;
    onAction1: () => void;
    onAction2: () => void;
    onAction3: () => void;
}

class Container extends React.Component<Props, any> {
    render() {
        const {onAction1, onAction2, onAction3, state} = this.props;

        return (
            <div>
                <div>
                    <Chip>{state.value1}</Chip>
                    <Chip>{state.value2}</Chip>
                </div>
                <div>
                    <RaisedButton label="Action1" onClick={onAction1}/>
                    <RaisedButton label="Action2" onClick={onAction2}/>
                    <RaisedButton label="Action3" onClick={onAction3}/>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        state: state.state1
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onAction1: (): void => dispatch({type: Action1Request}),
        onAction2: (): void => dispatch({type: Action2Request}),
        onAction3: (): void => dispatch({type: Action3Request})
    };
};


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Container);




import {ActionsObservable, combineEpics, Epic} from "redux-observable";
import {Observable} from "rxjs";
import "rxjs";

import {Action1, Action1Request, Action2, Action2Request, Action3, Action3Request} from "./Action";

export const EpicAction1: Epic<any, any> = (action$: ActionsObservable<any>): Observable<any> => {
    return action$.ofType(Action1Request).map((a) => {
        console.log(a);
        return {type: Action1, payload: Math.random().toString(36).substring(7)};
    });
};

export const EpicAction2: Epic<any, any> = (action$: ActionsObservable<any>): Observable<any> => {
    return action$.ofType(Action2Request).map((a) => {
        console.log(a);
        return {type: Action2, payload: Math.random().toString(36).substring(7)};
    });
};

export const EpicAction3: Epic<any, any> = (action$: ActionsObservable<any>): Observable<any> => {
    return action$.ofType(Action3Request).map((a) => {
        console.log(a);
        return {type: Action3, payload: Math.random().toString(36).substring(7)};
    });
};

const Epic = combineEpics(EpicAction1, EpicAction2, EpicAction3);

export default Epic;

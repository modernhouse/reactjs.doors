import {Action1, Action2, Action3} from "./Action";

export interface State {
    value1: number
    value2: string
    value3: string[]
}

export const Reducer = (state: State = {value1: null, value2: null, value3: []}, action) => {
    switch (action.type) {
        case Action1:
            return Object.assign({}, state, {value1: action.payload});
        case Action2:
            return Object.assign({}, state, {value2: action.payload});
        case Action3:
        default:
            return state;
    }
};


export default Reducer;

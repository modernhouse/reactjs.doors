const path = require("path");

const config = {
    entry: [path.resolve(__dirname, "app.tsx")],
    output: {
        path: path.resolve(__dirname, "build"),
        publicPath: "/assets/",
        filename: "bundle.js"
    },
    resolve: {
        modules: [
            path.resolve(__dirname, './'),
            path.resolve(__dirname, '../../../node_modules')
        ],
        extensions: [".ts", ".tsx", ".js", ".css"]
    },
    module: {
        rules: [
            {
                test: /\.scss$/,
                loaders: ['style-loader', 'css-loader', 'sass-loader'],
                include: [
                    path.resolve(__dirname, "./"),
                ]
            },
            {
                test: /\.tsx?$/,
                loader: 'ts-loader',
                options: {
                    configFileName: "./tsconfig.json",
                },
                include: [
                    path.resolve(__dirname, "./"),
                ]
            },
            {
                enforce: "pre", test: /\.js$/, loader: "source-map-loader"
            }
        ]
    },
    devServer: {
        inline: false
    },
    devtool: 'source-map',
};

module.exports = config;

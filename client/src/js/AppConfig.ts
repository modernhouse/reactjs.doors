import Loglevel from "loglevel";

export const LOG_LEVEL = Loglevel.levels.INFO;
Loglevel.setDefaultLevel(LOG_LEVEL);

export const CANVAS_WIDTH: number = 700;
export const CANVAS_HEIGHT: number = 850;

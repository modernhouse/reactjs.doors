import {ActionsObservable, Epic, combineEpics} from "redux-observable";
import {Observable} from "rxjs";
import "rxjs";

import {MaterialTypes} from "model/MaterialTypes";
import {loadFillTypes, loadProfileTypes} from "services/MaterialsService";
import {State} from "services/StateService";
import Type = MaterialTypes.Type

export const ProfileTypesLoad: string = "Material.Action.ProfileTypesLoad";
export const FillTypesLoad: string = "Material.Action.FillTypesLoad";
export const ProfileTypesLoaded: string = "Material.Action.ProfileTypesLoaded";
export const FillTypesLoaded: string = "Material.Action.FillTypesLoaded";


const epicLoadFillTypes: Epic<any, State> = (action$: ActionsObservable<any>): Observable<any> => {
    return action$.ofType(FillTypesLoad).map((action: any) => {
            let types = loadFillTypes();
            return {type: FillTypesLoaded, types: types}
        }
    );
};


const epicLoadProfileTypes: Epic<any, State> = (action$: ActionsObservable<any>): Observable<any> => {
    return action$.ofType(ProfileTypesLoad).map((action: any) => {
            let types = loadProfileTypes();
            return {type: ProfileTypesLoaded, types: types}
        }
    );
};

export const MaterialEpic = combineEpics(epicLoadFillTypes, epicLoadProfileTypes);

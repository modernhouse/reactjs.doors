export const OpenLeft: string = "Drawer/Action/OpenLeft";
export const OpenRight: string = "Drawer/Action/OpenRight";
export const CloseLeft: string = "Drawer/Action/CloseLeft";
export const CloseRight: string = "Drawer/Action/CloseRight";

export type DrawerAction = {
    type: string
}
//Door
import {requestSuffix} from "actions/ActionService";
import {ModelTypes} from "model/ModelTypes";
import {Action} from "redux";
import Cell = ModelTypes.Cell;
import Split = ModelTypes.Split;
import Door = ModelTypes.Door;

export const AddDoor: string = 'Model.Action.AddDoor';
export const AddDoorRequest: string = requestSuffix(AddDoor);

export const DeleteDoor: string = 'Model.Action.DeleteDoor';
export const DeleteDoorRequest: string = requestSuffix(DeleteDoor);

export const UpdateDoor: string = 'Model.Action.UpdateDoor';
export const UpdateDoorRequest: string = requestSuffix(UpdateDoor);

//Cell
export const AddCell: string = 'Model.Action.AddCell';
export const UpdateCell: string = 'Model.Action.UpdateCell';
export const UpdateCellRequest: string = requestSuffix(UpdateCell);
export const UpdateCellSize: string = 'Model.Action.UpdateCellSize';
export const UpdateCellSizeRequest: string = requestSuffix(UpdateCellSize);
export const DeleteCell: string = 'Model.Action.DeleteCell';


export const _AddDoor = () => ({type: AddDoorRequest});
export const _UpdateDoor = (changes: any) => ({type: UpdateDoorRequest, changes: changes});
export const _DeleteDoor = (id: string) => ({type: DeleteDoorRequest, id: id});

export const _UpdateCell = (id: string, changes: any) => ({type: UpdateCellRequest, id: id, changes: changes});
export const _UpdateCellSize = (id: string, changes: any) => ({type: UpdateCellSizeRequest, id: id, changes: changes});


//actions types
export interface MAction extends Action {
    id: string
    changes?: any
}

export type ActionUpdateDoor = {
    type: string, doorId: string, door: Door
}

export type ActionUpdateCell = {
    type: string, doorId: string, cellId: string, cell: Cell
}


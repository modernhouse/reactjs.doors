import {MiddlewareAPI} from "redux";
import {ActionsObservable, combineEpics, Epic} from "redux-observable";
import {Observable} from "rxjs";
import "rxjs";


import * as Lodash from "lodash";

import * as DoorService from "services/DoorService";
import * as StateService from "services/StateService";
import {State} from "services/StateService";

import * as DrawerActions from "actions/drawer/DrawerActions";
import * as ModelActions from "actions/model/ModelActions";
import SplitActions from "actions/model/SplitActions";


import {ModelTypes} from "model/ModelTypes";
import {MaterialTypes} from "model/MaterialTypes";

import * as SelectionActions from "actions/selection/SelectionActions";

import DoorEpic from "actions/model/epic/door/index";
import CellEpic from "actions/model/epic/cell/index";
import SplitEpic from "actions/model/epic/split/index";



import * as BuildUpdateSplit from "actions/model/epic/split/BuildUpdateSplit"

import Cell = ModelTypes.Cell
import Split = ModelTypes.Split
import SplitType = ModelTypes.SplitType
import Type = MaterialTypes.Type
import Door = ModelTypes.Door;
import Selection = ModelTypes.Selection;
import Model = ModelTypes.Model;


export const deleteSplit = (splitId: string) => {
    return (dispatch, getState) => {
        let door: Door = StateService.getSelectedDoor(getState());
        let split: Split = DoorService.getSplit(door, splitId);
        let cell: Cell = DoorService.getCellBySplitId(door, splitId);
        split.cellIds.map((id) => DoorService.getCell(door, id))
            .filter((c: Cell) => !Lodash.isNil(c.splitId)).forEach((c: Cell) => {
            dispatch(deleteSplit(c.splitId));
        });

        dispatch({type: SplitActions.Delete, doorId: door.id, splitId: splitId});
        dispatch(SelectionActions._DeselectAllCells());
        dispatch(SelectionActions._SelectCell(cell.id));
    }
};

export const updateSplit = (splitId: string, changes: any) => {
    return (dispatch, getState) => {
        let door: Door = StateService.getSelectedDoor(getState());
        let action: SplitActions.ActionUpdate = BuildUpdateSplit.buildAction(door, splitId, changes);
        dispatch(action);
        action.cells.forEach((cell: Cell) => {
            dispatch(updateCellSize(cell.id, {size: cell.size.clone(), pos: cell.pos.clone()}))
        })
    }
};


export const updateCellSize = (cellId: string, changes: any) => {
    return (dispatch, getState) => {
        let door: Door = StateService.getSelectedDoor(getState());
        let cell: Cell = DoorService.getCell(door, cellId);
        let x: number = cell.size.x;
        let y: number = cell.size.y;
        cell = Object.assign({}, cell, changes);

        x = cell.size.x / x;
        y = cell.size.y / y;

        dispatch({
            type: ModelActions.UpdateCell,
            doorId: door.id,
            cellId: cell.id,
            cell: cell
        });

        door = StateService.getSelectedDoor(getState());
        cell = DoorService.getCell(door, cellId);
        if (!Lodash.isNil(cell.splitId)) {
            let split: Split = DoorService.getSplit(door, cell.splitId);
            let offset = split.offset;
            switch (split.type) {
                case SplitType.horizontal:
                    offset = offset * y;
                    break;
                case SplitType.vertical:
                    offset = offset * x;
                    break;
                default:
                    break;
            }
            dispatch(updateSplit(split.id, {offset: Math.round(offset)}));
        }
    }
};

export const ModelEpic = combineEpics(DoorEpic, CellEpic, SplitEpic);

import {requestSuffix} from "actions/ActionService";
import {ModelTypes} from "model/ModelTypes";
import Split = ModelTypes.Split;
import Cell = ModelTypes.Cell;
import SplitType = ModelTypes.SplitType;


namespace SplitActions {
    export const Add: string = 'Model.Action.Split.Add';
    export const AddRequest: string = requestSuffix(Add);

    export const Delete: string = 'Model.Action.Split.Delete';
    export const DeleteRequest: string = requestSuffix(Delete);

    export const Update: string = 'Model.Action.Split.Update';
    export const UpdateRequest: string = requestSuffix(Update);

    export const _Update = (id: string, changes: any) => ({type: UpdateRequest, id: id, changes: changes});
    export const _Add = (type: SplitType) => ({type: AddRequest, splitType: type});
    export const _Delete = (id: string) => ({type: DeleteRequest, id: id});

    export type ActionUpdate = {
        type: string, doorId: string, cellId: string, split: Split, cells: Cell[]
    }
    export type ActionAdd = {
        type: string, doorId: string, cellId: string, split: Split, cells: Cell[]
    }
    export type ActionDelete = {
        type: string, doorId: string, splitId: string
    }
}

export default SplitActions



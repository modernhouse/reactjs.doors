import {MiddlewareAPI} from "redux";
import {ActionsObservable, Epic} from "redux-observable";
import {Observable} from "rxjs";
import "rxjs";


import * as Lodash from "lodash";

import * as DoorService from "services/DoorService";
import * as StateService from "services/StateService";
import {State} from "services/StateService";

import {ModelTypes} from "model/ModelTypes";
import {MaterialTypes} from "model/MaterialTypes";

import Cell = ModelTypes.Cell
import Split = ModelTypes.Split
import SplitType = ModelTypes.SplitType
import Type = MaterialTypes.Type
import Door = ModelTypes.Door;
import Selection = ModelTypes.Selection;
import Model = ModelTypes.Model;

import * as ModelActions from "actions/model/ModelActions";
import SplitActions from "actions/model/SplitActions";


const CellSizeUpdateEpic: Epic<any, State> = (action$: ActionsObservable<any>, store: MiddlewareAPI<State>): Observable<any> => {
    return action$.ofType(ModelActions.UpdateCellSizeRequest).flatMap((action) => {
        let result = [];
        let door: Door = StateService.getSelectedDoor(store.getState());
        let cell: Cell = DoorService.getCell(door, action.id);
        let dX: number = cell.size.x;
        let dY: number = cell.size.y;

        cell = Object.assign({}, cell, action.changes);
        dX = cell.size.x / dX;
        dY = cell.size.y / dY;

        result.push({
            type: ModelActions.UpdateCell, doorId: door.id,
            cellId: cell.id, cell: cell
        });

        if (!Lodash.isNil(cell.splitId)) {
            let split: Split = DoorService.getSplit(door, cell.splitId);
            let offset = split.offset;
            switch (split.type) {
                case SplitType.horizontal:
                    offset = offset * dY;
                    break;
                case SplitType.vertical:
                    offset = offset * dX;
                    break;
                default:
                    break;
            }
            result.push(SplitActions._Update(split.id, {offset: Math.round(offset)}));
        }
        return result;
    });
};

export default CellSizeUpdateEpic;
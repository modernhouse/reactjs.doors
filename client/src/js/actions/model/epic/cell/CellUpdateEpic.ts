import {MiddlewareAPI} from "redux";
import {ActionsObservable, combineEpics, Epic} from "redux-observable";
import {Observable} from "rxjs";
import "rxjs";
import * as Lodash from "lodash";

import * as DoorService from "services/DoorService";
import * as StateService from "services/StateService";
import {State} from "services/StateService";

import {ModelTypes} from "model/ModelTypes";
import {MaterialTypes} from "model/MaterialTypes";

import Cell = ModelTypes.Cell
import Split = ModelTypes.Split
import SplitType = ModelTypes.SplitType
import Type = MaterialTypes.Type
import Door = ModelTypes.Door;
import Selection = ModelTypes.Selection;
import Model = ModelTypes.Model;

import * as ModelActions from "actions/model/ModelActions";

const CellUpdateEpic: Epic<any, State> = (action$: ActionsObservable<any>, store: MiddlewareAPI<State>): Observable<any> => {
    return action$.ofType(ModelActions.UpdateCellRequest).map((action) => {
        let door: Door = StateService.getSelectedDoor(store.getState());
        let cell: Cell = Lodash.cloneDeep(DoorService.getCell(door, action.id));
        cell = Object.assign({}, cell, action.changes);
        return {type: ModelActions.UpdateCell, doorId: door.id, cellId: cell.id, cell: cell};
    })
};

export default CellUpdateEpic
import {combineEpics} from "redux-observable";

import CellSizeUpdateEpic from "./CellSizeUpdateEpic";
import CellUpdateEpic from "./CellUpdateEpic";

const CellEpic = combineEpics(CellSizeUpdateEpic, CellUpdateEpic);

export default CellEpic;

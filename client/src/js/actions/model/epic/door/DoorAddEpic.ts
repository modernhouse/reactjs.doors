import {MiddlewareAPI} from "redux";
import {ActionsObservable, Epic} from "redux-observable";
import {Observable} from "rxjs";
import "rxjs";

import * as DoorService from "services/DoorService";
import {State} from "services/StateService";
import {MaterialTypes} from "model/MaterialTypes";

import {ModelTypes} from "model/ModelTypes";
import * as ModelActions from "actions/model/ModelActions";
import * as SelectionActions from "actions/selection/SelectionActions";
import * as DrawerActions from "actions/drawer/DrawerActions";
import Cell = ModelTypes.Cell
import Split = ModelTypes.Split
import SplitType = ModelTypes.SplitType
import Type = MaterialTypes.Type
import Door = ModelTypes.Door;
import Selection = ModelTypes.Selection;
import Model = ModelTypes.Model;


const DoorAddEpic: Epic<any, State> = (action$: ActionsObservable<any>, store: MiddlewareAPI<State>): Observable<any> => {
    return action$.ofType(ModelActions.AddDoorRequest).flatMap(action => {
        let door: Door = DoorService.createDoor(store.getState().model.doors.length + 1);
        return [{type: ModelActions.AddDoor, door: door},
            SelectionActions._SelectDoor(door.id)];
            // {type: DrawerActions.CloseLeft},
            // {type: DrawerActions.OpenRight}];
    })
};

export default DoorAddEpic;
import {MiddlewareAPI} from "redux";
import {ActionsObservable, combineEpics, Epic} from "redux-observable";
import {Observable} from "rxjs";
import "rxjs";

import {State} from "services/StateService";
import {MaterialTypes} from "model/MaterialTypes";

import {ModelTypes} from "model/ModelTypes";
import Cell = ModelTypes.Cell
import Split = ModelTypes.Split
import SplitType = ModelTypes.SplitType
import Type = MaterialTypes.Type
import Door = ModelTypes.Door;
import Selection = ModelTypes.Selection;
import Model = ModelTypes.Model;

import * as ModelActions from "actions/model/ModelActions";
import * as SelectionActions from "actions/selection/SelectionActions";

const DoorDeleteEpic: Epic<any, State> = (action$: ActionsObservable<any>, store: MiddlewareAPI<State>): Observable<any> => {
    return action$.ofType(ModelActions.DeleteDoorRequest).flatMap((action) => {
        return [
            {type: ModelActions.DeleteDoor, id: action.id},
            {type: SelectionActions.SelectLastDoorRequest}
        ]
    })
};

export default DoorDeleteEpic;
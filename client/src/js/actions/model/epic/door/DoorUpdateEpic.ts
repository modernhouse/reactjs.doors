import {MiddlewareAPI} from "redux";
import {ActionsObservable, Epic} from "redux-observable";
import {Observable} from "rxjs";
import "rxjs";

import * as Lodash from "lodash";

import * as StateService from "services/StateService";
import {MaterialTypes} from "model/MaterialTypes";

import {ModelTypes} from "model/ModelTypes";
import * as ModelActions from "actions/model/ModelActions";
import Cell = ModelTypes.Cell
import Split = ModelTypes.Split
import SplitType = ModelTypes.SplitType
import Type = MaterialTypes.Type
import Door = ModelTypes.Door;
import Selection = ModelTypes.Selection;
import Model = ModelTypes.Model;

const DoorUpdateEpic: Epic<any, StateService.State> = (action$: ActionsObservable<any>, store: MiddlewareAPI<StateService.State>): Observable<ModelActions.ActionUpdateDoor> => {
    return action$.ofType(ModelActions.UpdateDoorRequest).map((action) => {
        let door: Door = Lodash.cloneDeep(StateService.getSelectedDoor(store.getState()));
        door = Object.assign({}, door, action.changes);
        return {
            type: ModelActions.UpdateDoor,
            doorId: door.id,
            door: door
        }
    });
};

export default DoorUpdateEpic;
import {combineEpics} from "redux-observable";

import DoorAddEpic from "./DoorAddEpic";
import DoorDeleteEpic from "./DoorDeleteEpic";
import DoorUpdateEpic from "./DoorUpdateEpic";

const DoorEpic = combineEpics(DoorAddEpic, DoorDeleteEpic, DoorUpdateEpic);

export default DoorEpic;

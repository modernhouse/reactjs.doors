import * as Lodash from "lodash";

import {ModelTypes} from "model/ModelTypes";
import * as DoorService from "services/DoorService";
import * as SplitService from "services/SplitService";
import SplitActions from "actions/model/SplitActions";


import Cell = ModelTypes.Cell;
import Split = ModelTypes.Split;
import Door = ModelTypes.Door;
import SplitType = ModelTypes.SplitType;


export const buildAction = (door: Door, splitId: string, changes: any): SplitActions.ActionUpdate => {
    let split: Split = DoorService.getSplit(door, splitId);
    split = Object.assign({}, Lodash.cloneDeep(split), changes);

    let cell: Cell = DoorService.getCellBySplitId(door, splitId);

    let request: SplitService.SplitRequest = {
        cell: cell,
        split: split,
        cells: split.cellIds.map((id) => Lodash.cloneDeep(DoorService.getCell(door, id)))
    };

    let result: SplitService.SplitResult = SplitService.processSplit(request);

    return {
        type: SplitActions.Update,
        doorId: door.id,
        cellId: cell.id,
        split: result.split,
        cells: result.cells
    };
};

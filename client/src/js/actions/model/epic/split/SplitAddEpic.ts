import {MiddlewareAPI} from "redux";
import {ActionsObservable, Epic} from "redux-observable";
import {Observable} from "rxjs";
import * as StateService from "services/StateService";
import {State} from "services/StateService";
import * as SplitService from "services/SplitService";

import * as SelectionActions from "actions/selection/SelectionActions";
import SplitActions from "actions/model/SplitActions";
import * as DoorService from "services/DoorService";


import {ModelTypes} from "model/ModelTypes";
import Door = ModelTypes.Door;
import SplitType = ModelTypes.SplitType;
import Cell = ModelTypes.Cell;
import Split = ModelTypes.Split;

const createAction = (door: Door, cellId: string, splitType: SplitType): any[] => {
    let action: SplitActions.ActionAdd = {
        type: SplitActions.Add,
        doorId: door.id,
        cellId: cellId,
        split: null,
        cells: null
    };

    let cell: Cell = DoorService.getCell(door, cellId);

    let cells: Cell[] = [DoorService.createCell(cell.fill), DoorService.createCell(cell.fill)];

    let split: Split = DoorService.createSplit(cell, splitType, cells);

    let request: SplitService.SplitRequest = {
        cell: cell,
        split: split,
        cells: cells
    };

    let result: SplitService.SplitResult = SplitService.processSplit(request);
    action.split = result.split;
    action.cells = result.cells;

    let actions: any[] = [action, SelectionActions._DeselectAllCells()];
    actions.push(...action.cells.map((cell) => SelectionActions._SelectCell(cell.id)));
    return actions;
};

const epic: Epic<any, State> = (action$: ActionsObservable<any>, store: MiddlewareAPI<State>): Observable<any> => {
    return action$.ofType(SplitActions.AddRequest).flatMap((action) => {

        let cellIds: string[] = store.getState().selection.cellIds;

        let door: Door = StateService.getSelectedDoor(store.getState());

        let result = [];
        let actions = cellIds.map((id) => createAction(door, id, action.splitType)).reduce((a, b) => a.concat(b));
        result.push(...actions);
        return result;
    });
};


export default epic;
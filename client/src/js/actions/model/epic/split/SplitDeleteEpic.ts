import * as Lodash from "lodash";

import {MiddlewareAPI} from "redux";
import {ActionsObservable, Epic} from "redux-observable";

import {Observable} from "rxjs/Observable";

import * as StateService from "services/StateService";
import {State} from "services/StateService";

import SplitActions from "actions/model/SplitActions";
import * as DoorService from "services/DoorService";
import * as SelectionActions from "actions/selection/SelectionActions";


import {ModelTypes} from "model/ModelTypes";
import Door = ModelTypes.Door;
import SplitType = ModelTypes.SplitType;
import Cell = ModelTypes.Cell;
import Split = ModelTypes.Split;

const epic: Epic<any, State> = (action$: ActionsObservable<any>, store: MiddlewareAPI<State>): Observable<any> => {
    return action$.ofType(SplitActions.DeleteRequest).flatMap((action) => {
        let result = [];

        let door: Door = StateService.getSelectedDoor(store.getState());
        let split: Split = DoorService.getSplit(door, action.id);
        let cell: Cell = DoorService.getCellBySplitId(door, split.id);


        let actions = split.cellIds.map((id) => DoorService.getCell(door, id))
            .filter((c: Cell) => !Lodash.isNil(c.splitId))
            .map((c) => SplitActions._Delete(c.splitId));


        result.push(...actions);
        result.push({type: SplitActions.Delete, doorId: door.id, splitId: split.id});
        result.push(SelectionActions._DeselectAllCells());
        result.push(SelectionActions._SelectCell(cell.id));
        return result;
    })
};

export default epic;


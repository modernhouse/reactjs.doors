import * as Lodash from "lodash";

import {MiddlewareAPI} from "redux";
import {ActionsObservable, Epic} from "redux-observable";

import {Observable} from "rxjs/Observable";

import * as SplitService from "services/SplitService";
import * as StateService from "services/StateService";
import {State} from "services/StateService";

import * as DoorService from "services/DoorService";
import SplitActions from "actions/model/SplitActions";
import * as ModelActions from "actions/model/ModelActions";


import {ModelTypes} from "model/ModelTypes";
import Door = ModelTypes.Door;
import SplitType = ModelTypes.SplitType;
import Cell = ModelTypes.Cell;
import Split = ModelTypes.Split;

const epic: Epic<any, State> = (action$: ActionsObservable<any>, store: MiddlewareAPI<State>): Observable<any> => {
    return action$.ofType(SplitActions.UpdateRequest).flatMap(action => {

        let door: Door = StateService.getSelectedDoor(store.getState());

        let split: Split = DoorService.getSplit(door, action.id);
        split = Object.assign({}, Lodash.cloneDeep(split), action.changes);

        let cell: Cell = DoorService.getCellBySplitId(door, split.id);

        let request: SplitService.SplitRequest = {
            cell: cell,
            split: split,
            cells: split.cellIds.map((id) => Lodash.cloneDeep(DoorService.getCell(door, id)))
        };

        let result: SplitService.SplitResult = SplitService.processSplit(request);


        let actions: any = [{
            type: SplitActions.Update, doorId: door.id, cellId: cell.id,
            split: result.split, cells: result.cells
        }];


        actions.push(... result.cells.map((cell: Cell) => ModelActions._UpdateCellSize(cell.id, {
            size: cell.size.clone(),
            pos: cell.pos.clone()
        })));
        return actions;
    })
};

export default epic;

import {combineEpics} from "redux-observable";

import SplitAddEpic from "./SplitAddEpic";
import SplitDeleteEpic from "./SplitDeleteEpic";
import SplitUpdateEpic from "./SplitUpdateEpic";

const SplitEpic = combineEpics(SplitDeleteEpic, SplitAddEpic, SplitUpdateEpic);

export default SplitEpic;

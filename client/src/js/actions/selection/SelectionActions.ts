import {ModelTypes} from "model/ModelTypes";
import {requestSuffix} from "actions/ActionService";
import Door = ModelTypes.Door;
import Split = ModelTypes.Split;
import Cell = ModelTypes.Cell;

export const SelectDoor: string = 'Selection.Action.SelectDoor';
export const SelectDoorRequest: string = requestSuffix(SelectDoor);
export const SelectLastDoorRequest: string = requestSuffix('Selection.Action.SelectLastDoor');

export const DeselectDoor: string = 'Selection.Action.UnselectDoor';

export const SelectSplit: string = 'Selection.Action.SelectSplit';
export const DeselectSplit: string = 'Selection.Action.DeselectSplit';

export const SelectCell: string = 'Selection.Action.SelectCell';
export const DeselectCell: string = 'Selection.Action.DeselectCell';
export const DeselectAllCells: string = 'Selection.Action.DeselectAllCells';

export const _SelectDoor = (id: string) => ({type: SelectDoorRequest, doorId: id});

export const _SelectCell = (id: string) => ({type: SelectCell, id: id});

export const _DeselectCell = (id: string) => ({type: DeselectCell, id: id});

export const _DeselectAllCells = () => ({type: DeselectAllCells});

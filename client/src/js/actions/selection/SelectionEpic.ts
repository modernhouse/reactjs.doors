import {ActionsObservable, combineEpics, Epic} from "redux-observable";
import {Observable} from "rxjs";
import "rxjs";
import * as Lodash from "lodash";


import {State} from "services/StateService";
import * as ModelService from "services/ModelService";

import {MiddlewareAPI} from "redux";
import {ModelTypes} from "model/ModelTypes";
import * as SelectionActions from "actions/selection/SelectionActions";
import Split = ModelTypes.Split;
import Door = ModelTypes.Door;

const selectDoorEpic: Epic<any, State> = (action$: ActionsObservable<any>, store: MiddlewareAPI<State>): Observable<any> => {
    return action$.ofType(SelectionActions.SelectDoorRequest).flatMap((action) => {

        let door: Door = ModelService.getDoor(store.getState().model, action.doorId);
        let actions = [];
        actions.push({type: SelectionActions.DeselectAllCells}, {type: SelectionActions.SelectDoor, doorId: door.id});
        let split: Split = Lodash.last(door.splits);

        if (Lodash.isNil(split)) {
            actions.push({type: SelectionActions.SelectCell, id: door.cells[0].id})
        } else {
            actions.push(...split.cellIds.map(id => ({type: SelectionActions.SelectCell, id: id})));
        }
        return actions;
    })
};

const selectLastDoorEpic: Epic<any, State> = (action$: ActionsObservable<any>, store: MiddlewareAPI<State>): Observable<any> => {
    return action$.ofType(SelectionActions.SelectLastDoorRequest).flatMap((action) => {
        let door: Door = ModelService.lastDoor(store.getState().model);
        return Lodash.isNil(door) ? [] : [SelectionActions._SelectDoor(door.id)]
    })
};


export const SelectionEpic = combineEpics(selectDoorEpic, selectLastDoorEpic);

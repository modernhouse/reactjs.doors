import * as React from "react";
import * as ReactDOM from "react-dom";
import {applyMiddleware, combineReducers, createStore} from "redux";
import {combineEpics, createEpicMiddleware} from "redux-observable"
import {Provider} from "react-redux";

import injectTapEventPlugin from "react-tap-event-plugin";
import {Button} from "reactstrap";
import {Tab, Tabs} from "material-ui/Tabs";

import ProfileTypesReducer from "reducers/ProfileTypesReducer";
import ModelReducer from "reducers/ModelReducer";
import SelectionReducer from "reducers/SelectionReducer";
import FillTypesReducer from "reducers/FillTypesReducer";
import {DrawerReducer} from "reducers/DrawerReducer";
import {ModelTypes} from "model/ModelTypes";


import MainContainer from "containers/MainContainer";

import "../scss/theme.scss";

import {MaterialTypes} from "model/MaterialTypes";
import Door = ModelTypes.Door;
import Model = ModelTypes.Model;
import Type = MaterialTypes.Type;

import {MaterialEpic} from "actions/MaterialActions";
import {ModelEpic} from "actions/model/ModelActionsService";
import {SelectionEpic} from "actions/selection/SelectionEpic";


const rootId: string = "root";

document.body.style.zoom = "75%";

injectTapEventPlugin();


const rootEpic = combineEpics(MaterialEpic, ModelEpic, SelectionEpic);

const store = createStore(combineReducers({
    model: ModelReducer,
    selection: SelectionReducer,
    profileTypes: ProfileTypesReducer,
    fillTypes: FillTypesReducer,
    drawer: DrawerReducer
}), applyMiddleware(createEpicMiddleware(rootEpic)));


const render = () => ReactDOM.render(
    <Provider store={store}>
        <MainContainer/>
    </Provider>,
    document.getElementById(rootId)
);

render();


store.subscribe(render);

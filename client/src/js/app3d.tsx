import * as React from "react";
import * as ReactDOM from "react-dom";
import {ModelTypes} from "./model/ModelTypes";

import * as THREE from "three";


import Editor3DContainer from "./containers/Editor3DContainer";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import injectTapEventPlugin from "react-tap-event-plugin";

import Door = ModelTypes.Door;
import SplitType = ModelTypes.SplitType;
import Cell = ModelTypes.Cell;

injectTapEventPlugin();

const door: Door = {
    id: '1',
    name: 'door 1',
    count: 1,
    profile: {},
    cells: [
        {
            id: '1',
            size: new THREE.Vector3(1000,2000, 100),
            pos: new THREE.Vector3(),
            fill: {
                typeId: '',
                codeId: ''
            }
        }
    ],
    splits: []
};


const render = () => ReactDOM.render(
    <MuiThemeProvider>
        <Editor3DContainer door={door}/>
    </MuiThemeProvider>,
    document.getElementById("root")
);


render();

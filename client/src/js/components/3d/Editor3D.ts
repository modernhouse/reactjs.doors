import * as THREE from "three";
import {OrbitControls} from "three-orbitcontrols-ts"
import * as React from "react";
import * as Validators from "model/Validators";

import Style from "services/3d/Style";

import CellMesh from "services/3d/CellMesh";

import * as Lodash from "lodash";
import Loglevel from "loglevel";
import {LOG_LEVEL} from "AppConfig";
import {ModelTypes} from "model/ModelTypes";
import {SplitMesh} from "services/3d/SplitMesh";
import {DoorService} from "services/DoorService";


import Component = React.Component;
import LineSegments = THREE.LineSegments;

import Vector3 = THREE.Vector3;
import Box3 = THREE.Box3;
import randFloatSpread = THREE.Math.randFloatSpread;

import Cell = ModelTypes.Cell;
import Door = ModelTypes.Door;
import Split = ModelTypes.Split;
import SplitType = ModelTypes.SplitType;


const logger = Loglevel.getLogger('Editor3D');
logger.setLevel(LOG_LEVEL);

//TODO need to understand how it could be calculated
const VIEW_SIZE_RATE = 1.7;

export default class Editor {
    state: {
        view: View
        door: Door
    };

    root: HTMLCanvasElement;
    renderer: THREE.WebGLRenderer;
    scene: THREE.Scene;

    aspect: number;
    viewSize: number;
    cameraP: THREE.PerspectiveCamera;
    cameraO: THREE.OrthographicCamera;


    gridXY: THREE.GridHelper;
    gridYZ: THREE.GridHelper;
    gridZX: THREE.GridHelper;

    doorMesh: CellMesh;

    doorGroup: THREE.Group;

    geometry: THREE.BoxGeometry;

    controls: OrbitControls;

    rotVelocities = {
        x: 0.01,
        y: 0.01,
        z: 0.01
    };

    cameraHelper: THREE.CameraHelper;

    constructor(root: HTMLElement, w: number, h: number) {
        this.root = <HTMLCanvasElement>root;

        this.renderer = new THREE.WebGLRenderer({
            canvas: this.root,
            alpha: true,
            antialias: true,
        });
        this.renderer.setSize(w, h);

        this.scene = new THREE.Scene();
        this.scene.background = new THREE.Color(0xcfd8dc);

        this.aspect = w / h;
        this.viewSize = Math.max(w, h);

        this.cameraP = new THREE.PerspectiveCamera(60, this.aspect, 1, 10000);

        this.cameraO = new THREE.OrthographicCamera(-this.viewSize * this.aspect / 2,
            this.viewSize * this.aspect / 2,
            this.viewSize * this.aspect / 2,
            -this.viewSize * this.aspect / 2, -this.viewSize / 2, this.viewSize / 2);
        //this.scene.add( this.cameraO );


        this.gridXY = new THREE.GridHelper(1000, 100);
        this.gridYZ = new THREE.GridHelper(1000, 100);
        this.gridYZ.rotateX(Math.PI / 2);
        this.gridZX = new THREE.GridHelper(1000, 100);
        this.gridYZ.rotateZ(Math.PI / 2);

        this.scene.add(this.gridXY);
        this.scene.add(this.gridYZ);
        this.scene.add(this.gridZX);

        try {
            this.controls = new OrbitControls(this.cameraO, this.root);
            this.controls.enableDamping = true;
            this.controls.dampingFactor = 0.25;
            this.controls.enableZoom = false;
            this.controls.addEventListener( 'change', ()=>{this.render()} );
        } catch (e) {
            console.log(e)
        }

        this.state = {
            view: View.front,
            door: null
        }
    }

    private refreshDoorMesh() {

        this.doorGroup = new THREE.Group();

        let material: THREE.MeshBasicMaterial = new THREE.MeshBasicMaterial({color: Style.cellColor, wireframe: false});

        this.state.door.splits.forEach((s: Split) => {
            this.doorGroup.add(new SplitMesh(s));
        });

        new DoorService(this.state.door).getVisibleCells().forEach((c: Cell, i: number) => {
                let mesh = new CellMesh(c, i + 1, material);
                this.doorGroup.add(mesh);
            }
        );

        this.scene.add(this.doorGroup);
    }

    private refreshCamera() {
        let cell: Cell = this.state.door.cells[0];
        let size: Vector3 = cell.size;

        this.viewSize = Math.max(size.x, size.y) * VIEW_SIZE_RATE;

        this.cameraP.far = this.viewSize;
        this.cameraP.updateProjectionMatrix();
        let position: Vector3 = this.getCameraPosition();
        this.cameraP.position.x = position.x;
        this.cameraP.position.y = position.y;
        this.cameraP.position.z = position.z;
        this.cameraP.lookAt(this.scene.position);

        this.updateCameraO();
    }

    private updateCameraO() {
        this.cameraO.left = -this.viewSize * this.aspect / 3;
        this.cameraO.right = this.viewSize * this.aspect / 3;
        this.cameraO.top = this.viewSize / 3;
        this.cameraO.bottom = -this.viewSize / 3;
        this.cameraO.near = -this.viewSize * 2;
        this.cameraO.far = this.viewSize * 2;

        let position: Vector3 = this.getCameraPosition();
        this.cameraO.position.x = position.x;
        this.cameraO.position.y = position.y;
        this.cameraO.position.z = position.z;
        this.cameraO.lookAt(this.scene.position);

        this.cameraO.updateProjectionMatrix();
    }

    private refreshGridHelper() {
        if (!Lodash.isNil(this.gridXY)) {
            this.scene.remove(this.gridXY);
            this.scene.remove(this.gridYZ);
            this.scene.remove(this.gridZX);
        }

        this.gridXY = new THREE.GridHelper(this.viewSize, 100);
        this.gridXY.rotateX(Math.PI / 2);


        this.gridYZ = new THREE.GridHelper(this.viewSize, 100);
        this.gridYZ.rotateZ(Math.PI / 2);

        this.gridZX = new THREE.GridHelper(this.viewSize, 100);
        //don't understand why I need it
        this.gridZX.position.y = 1;

        switch (this.state.view) {
            case View.front:
            case View.rear:
                this.scene.add(this.gridXY);
                break;
            case View.right:
            case View.left:
                this.scene.add(this.gridYZ);
                break;
            case View.top:
            case View.bottom:
                this.scene.add(this.gridZX);
                break;

        }
    }

    private getCameraPosition(): Vector3 {

        if (Validators.validateDoor(this.state.door)) {
            let cell: Cell = this.state.door.cells[0];
            let size: Vector3 = cell.size;
            let offset: number = Math.max(size.x, size.y);

            switch (this.state.view) {
                case View.front:
                    return new Vector3(0, 0, offset);
                case View.top:
                    return new Vector3(0, offset, 0);
                case View.right:
                    return new Vector3(offset, 0, 0);
                case View.rear:
                    return new Vector3(0, 0, -offset);
                case View.bottom:
                    return new Vector3(0, -offset, 0);
                case View.left:
                    return new Vector3(-offset, 0, 0);
                default:
                    throw new Error();
            }
        }
        return new Vector3(0, 0, 0);
    }


    render() {
        this.renderer.render(this.scene, this.cameraO);
    }

    private removedDoor() {
        if (!Lodash.isNil(this.doorMesh)) {
            this.scene.remove(this.doorMesh);
        }
        if (!Lodash.isNil(this.doorGroup)) {
            this.scene.remove(this.doorGroup);
        }
    }

    public setDoor(door: Door) {
        this.removedDoor();

        this.state = {
            view: this.state.view,
            door: door,
        };

        if (Validators.validateDoor(this.state.door)) {
            this.refreshDoorMesh();
            this.refreshCamera();
            this.refreshGridHelper();
        }
        this.render();
    }

    public resize(w: number, h: number): void {
        this.aspect = w / h;
        this.cameraP.aspect = this.aspect;
        this.cameraP.updateProjectionMatrix();

        this.updateCameraO();

        this.renderer.setSize(w, h);
        this.render();
    }

    public setView(view: View): void {
        this.state = {
            view: view,
            door: this.state.door
        };

        if (Validators.validateDoor(this.state.door)) {
            this.refreshCamera();
            this.refreshGridHelper();
        }
        this.render();
    }

}


export enum View {
    front,
    top,
    right,
    rear,
    bottom,
    left
}
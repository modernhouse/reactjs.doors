import * as React from "react";
import {Component} from "react";
import AutoComplete from "material-ui/AutoComplete";
import {MaterialTypes} from "../model/MaterialTypes";
import {formatCode} from "../services/MaterialsService";
import Type = MaterialTypes.Type;
import Code = MaterialTypes.Code;

interface Props {
    floatingLabelText: string
    hintText: string
    codeId: string
    codes: Code[]
    onChange: (string) => void
}

interface State {

}

const CodeConf = {
    text: 'text',
    value: 'code'
};


type DisplayCode = {
    text: string
    code: Code
}

class CodeField extends Component<Props, State> {
    field: AutoComplete;

    focus() {
        this.field.focus();
    }

    render() {
        const {codes, codeId, onChange, floatingLabelText, hintText} = this.props;

        const code:Code = codes.find((c) => c.id == codeId);

        const dcodes = codes.map((code: Code) => {
            return {
                text: formatCode(code),
                code: code
            }
        });


        const onNewRequest = (value: DisplayCode, index: number): void => {
            onChange(value.code.id);
        };
        return (

            <AutoComplete
                ref={(input) => this.field = input}
                floatingLabelText={floatingLabelText}
                hintText={hintText}
                dataSource={dcodes}
                dataSourceConfig={CodeConf}
                searchText={formatCode(code)}
                filter={AutoComplete.caseInsensitiveFilter}
                openOnFocus={true}
                onNewRequest={onNewRequest}
            />
        )
    }
}

export default CodeField;
import * as React from "react";
//material-ui
import {List, ListItem} from "material-ui/List";
import Avatar from "material-ui/Avatar";
import IconButton from "material-ui/IconButton";
import RemoveIcon from "material-ui/svg-icons/action/delete";
import DoorIcon from "material-ui/svg-icons/action/view-module";
import {blue500, yellow600, green600, cyan200} from "material-ui/styles/colors";
import Component = React.Component;

import {ModelTypes} from "../model/ModelTypes";
import Cell = ModelTypes.Cell;
import Door = ModelTypes.Door;
import Split = ModelTypes.Split;
import SplitType = ModelTypes.SplitType;
const style = {
    backgroundColor: cyan200
};

interface Props {
    door: Door,
    onDelete: (Door) => void,
    onSelect: (Door) => void,
    selected: boolean
}

class DoorListItem extends Component<Props, {}> {
    listItem: ListItem;
    buttonDelete: IconButton;

    onSelect(e: any): void {
        e.stopPropagation();
        this.props.onSelect(this.props.door);
    }

    onDelete(e: any): void {
        e.stopPropagation();
        this.props.onDelete(this.props.door);
    }

    render() {
        const {door, selected} = this.props;
        const onSelect = (e: any): void => this.onSelect(e);
        const onDelete = (e: any): void => this.onDelete(e);


        const leftAvatar = <Avatar icon={<DoorIcon/>} backgroundColor={blue500}/>;
        this.buttonDelete = <IconButton onClick={ onDelete }><RemoveIcon/></IconButton>;
        const rightIconButton = this.buttonDelete;
        const itemText = `${door.name}: ${door.count}`;
        return (
            <ListItem ref={(input) => this.listItem = input}
                      leftAvatar={ leftAvatar }
                      rightIconButton={ rightIconButton }
                      primaryText={itemText}
                      secondaryText={door.type}
                      onClick={ onSelect } style={selected ? style:null}/>
        )
    }
}


export default DoorListItem


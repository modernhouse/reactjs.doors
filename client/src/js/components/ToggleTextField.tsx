import * as React from "react";
import * as Lodash from "lodash";
import TextField from "material-ui/TextField";
import Toggle from "material-ui/Toggle";
import {inputs} from "./Styles";
import Component = React.Component;

class ToggleTextField extends Component<Props, State> {
    field: TextField;

    constructor() {
        super();
        this.state = { disabled: true};
    }

    componentWillReceiveProps = (nextProps: Readonly<Props>, nextContext: any): void => {
        this.setState({ disabled: Lodash.isNil(nextProps.value)});
    };


    componentDidUpdate(nextProps: Readonly<Props>, nextState: Readonly<State>, nextContext: any) {
        if (nextState.disabled) {
            this.field.focus()
        }
    }


    render() {
        const {hintText, type, floatingLabelText, value, onChange} = this.props;
        const disabled = this.state.disabled;
        const onToggle = (event: object, value: boolean): void => {
            if (!value)  {
                onChange(undefined);
            }
            this.setState({
                disabled: !value,
            });
        };

        return (
            <div>
                <div className="f-form-control">
                    <Toggle onToggle={onToggle} toggled={!disabled}/>
                </div>
                <div className="f-form-control">
                    <TextField ref={(input) => this.field = input} hintText={hintText} type={type}
                               floatingLabelText={floatingLabelText}
                               floatingLabelFixed={false}
                               disabled={disabled}
                               style={inputs.input}
                               value={Lodash.isNil(value) ? '': value}
                               onChange={(e, v) => onChange(v)}

                    />
                </div>
            </div>
        )
    }
}

interface State {
    disabled: boolean
}

interface Props {
    type: string
    hintText: string
    floatingLabelText: string
    value: any
    onChange: (string) => void
}

export default ToggleTextField

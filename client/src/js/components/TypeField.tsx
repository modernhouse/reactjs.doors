import * as React from "react";
import {Component} from "react";
import AutoComplete from "material-ui/AutoComplete";
import {MaterialTypes} from "../model/MaterialTypes";
import {formatType} from "../services/MaterialsService";
import Type = MaterialTypes.Type;

interface  Props {
    floatingLabelText: string
    hintText: string
    types: Type[]
    typeId: string
    onChange: (string) => void
}

interface State {

}

const typeConf = {
    text: 'name',
    value: 'id'
};

class TypeField extends Component<Props, State> {
    field: AutoComplete;

    focus() {
        this.field.focus();
    }

    render() {
        const {types, typeId, hintText, floatingLabelText, onChange} = this.props;
        const type: Type = types.find((t) => t.id == typeId);

        const onNewRequest = (value: Type, index: number): void => {
            onChange(value.id);
        };
        return (
            <AutoComplete
                ref={(input) => this.field = input}
                floatingLabelText={floatingLabelText}
                hintText={hintText}
                dataSource={types}
                dataSourceConfig={typeConf}
                searchText={formatType(type)}
                filter={AutoComplete.caseInsensitiveFilter}
                openOnFocus={true}
                onNewRequest={onNewRequest}
            />
        )
    }
}

export default TypeField;
import * as React from "react";
import {Component} from "react";
import * as Lodash from "lodash";
import TextField from "material-ui/TextField";
import ToggleTextField from "components/ToggleTextField";
import {ModelTypes} from "model/ModelTypes";
import Door = ModelTypes.Door;

interface Props {
    door: Door,
    updateDoor: (any) => void
}

class DoorDetails extends Component<Props, any> {
    fieldName: TextField;
    fieldCount: TextField;

    render() {
        const {door, updateDoor} = this.props;
        const onChangeName = (event: any, newValue: string): void => updateDoor({name: newValue});
        const onChangeCount = (event: any, newValue: string): void => updateDoor({count: Number(newValue)});
        const onChangeLength = (newValue: string): void => {
            if (Lodash.isNil(newValue)) {
                updateDoor({line: undefined});
            } else {
                updateDoor({line: {length: Number(newValue)}});
            }
        }
        return (
            <span>
                <div className="row left-40">
                    <div className="f-form-control">
                        <ToggleTextField hintText="Длинна" type="number"
                                         floatingLabelText="Верх/Низ направляющая"
                                         onChange={onChangeLength}
                                         value={Lodash.isNil(door.line) ? undefined : door.line.length}/>
                    </div>
                </div>
                <div className="row left-40">
                    <div className="f-form-control">
                        <TextField ref={(input) => this.fieldName = input}
                                   hintText="Название двери"
                                   floatingLabelText="Название"
                                   floatingLabelFixed={false}
                                   value={door.name} onChange={onChangeName}/>
                    </div>
                    <div className="f-form-control">
                        <TextField ref={(input) => this.fieldCount = input}
                                   hintText="Количество дверей" type="number"
                                   floatingLabelText="Количество"
                                   floatingLabelFixed={false}
                                   value={door.count} onChange={onChangeCount}
                                   autoFocus/>
                    </div>
                </div>
            </span>
        )
    }
}

export default DoorDetails
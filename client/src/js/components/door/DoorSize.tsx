import * as React from "react";
import * as THREE from "three";

import TextField from "material-ui/TextField";
import {ModelTypes} from "../../model/ModelTypes";
import Loglevel from "loglevel";
import {LOG_LEVEL} from "../../AppConfig";


import Component = React.Component;

import Cell = ModelTypes.Cell;
import Door = ModelTypes.Door;
import Split = ModelTypes.Split;
import SplitType = ModelTypes.SplitType;
import Vector3 = THREE.Vector3;

const logger = Loglevel.getLogger('CellView');
logger.setLevel(LOG_LEVEL);

interface Props {
    cell: Cell;
    onChange: (changes: any) => void;
}

export class DoorSize extends Component<Props, {}> {

    fieldHeight: TextField;
    fieldWidth: TextField;

    focus(): void {
        this.fieldHeight.focus();
    }

    render() {
        const {cell, onChange} = this.props;
        const onChangeWidth = (event: any, x: string): void => {
            onChange({size: cell.size.clone().setX(Number(x))});
        };
        const onChangeHeight = (event: any, y: string): void => {
            onChange({size: cell.size.clone().setY(Number(y))});
        };
        return (
            <div className="row left-40">
                <div className="f-form-control">
                    <TextField ref={(input) => this.fieldHeight = input}
                               type="number"
                               hintText="Введите высоту двери в мм"
                               floatingLabelText="Высота"
                               floatingLabelFixed={false}
                               value={cell.size.y}
                               onChange={onChangeHeight}
                    />
                </div>
                <div className="f-form-control">
                    <TextField
                        ref={(input) => this.fieldWidth = input}
                        type="number"
                        hintText="Введите ширину двери в мм"
                        floatingLabelText="Ширина"
                        floatingLabelFixed={false}
                        value={cell.size.x}
                        onChange={onChangeWidth}
                    />
                </div>
            </div>
        )
    }
}


export default DoorSize
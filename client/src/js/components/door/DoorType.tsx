import * as React from "react";
import {isNil} from "lodash";
import AutoComplete from "material-ui/AutoComplete";
import TypeField from "../TypeField";
import CodeField from "../CodeField";

import {RadioButton, RadioButtonGroup} from "material-ui/RadioButton";
import {ModelTypes} from "../../model/ModelTypes";
import {MaterialTypes} from "../../model/MaterialTypes";
import Component = React.Component;

import Cell = ModelTypes.Cell;
import Door = ModelTypes.Door;
import Split = ModelTypes.Split;
import SplitType = ModelTypes.SplitType;

import Type = MaterialTypes.Type;
import Code = MaterialTypes.Code;
import Profile = ModelTypes.Profile;

interface Props {
    //transfer focus to the next component
    nextFocus: () => void
    updateDoor: (any) => void
    door: Door
    types: Type[]
}

type DisplayCode = {
    text: string
    code: Code
}

class DoorType extends Component<Props, any> {
    fieldType: AutoComplete;
    fieldProfType: TypeField;
    fieldProfCode: CodeField;

    codes: Code[] = [];


    componentWillUpdate = (nextProps: Readonly<Props>, nextState: Readonly<any>, nextContext: any): void => {
        let door: Door = nextProps.door;
        let profile: Profile = door.profile;
        let types: Type[] = nextProps.types;
        this.codes = (isNil(profile) || isNil(profile.typeId)) ? [] : types.find((t) => t.id == profile.typeId).codes;
    };


    onChangeType = (value: string, index: number): void => {
        this.fieldProfType.focus();
        this.props.updateDoor({type: value});
    };

    onChangeProfileType = (typeId: string): void => {
        this.fieldProfCode.focus();
        this.props.updateDoor({
            profile: Object.assign({}, this.props.door.profile, {typeId: typeId})
        });
    };

    onChangeProfileCode = (codeId: string): void => {
        this.props.nextFocus();
        this.props.updateDoor({
                profile: Object.assign({}, this.props.door.profile, {codeId: codeId})
            }
        );
    };


    render() {
        const {door} = this.props;
        const onChangeType = this.onChangeType;
        const onChangeProfileType = this.onChangeProfileType;
        const onChangeProfileColor = this.onChangeProfileCode;
        const types = this.props.types;
        const typeId = door.profile.typeId;
        const codeId = door.profile.codeId;
        return (
            <div className="row left-40">
                <div className="f-form-control">
                    <AutoComplete
                        ref={(input) => this.fieldType = input}
                        floatingLabelText="Тип двери"
                        hintText="Выбирите тип двери"
                        dataSource={['Раздвижная', 'Распашная']}
                        filter={AutoComplete.caseInsensitiveFilter}
                        openOnFocus={true}
                        onNewRequest={onChangeType}
                        searchText={door.type}
                    />
                </div>
                <div className="f-form-control">
                    <TypeField
                        ref={(input) => this.fieldProfType = input}
                        floatingLabelText="Тип профиля"
                        hintText="Выбирите тип профиля"
                        types={types}
                        typeId={typeId}
                        onChange={onChangeProfileType}
                    />
                </div>
                <div className="f-form-control">
                    <CodeField
                        ref={(input) => this.fieldProfCode = input}
                        floatingLabelText="Цвет профиля"
                        hintText="Выбирите цвет профиля"
                        codes={this.codes}
                        codeId={codeId}
                        onChange={onChangeProfileColor}
                    />
                </div>
            </div>
        )
    }
}


export default DoorType;

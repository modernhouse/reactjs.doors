import * as React from "react";
import * as Lodash from "lodash";

import Avatar from "material-ui/Avatar";

import {blue50, blue500, cyan200, green600, yellow600} from "material-ui/styles/colors";
import {Card, CardActions, CardHeader, CardText} from "material-ui/Card";
import {ModelTypes} from "../../model/ModelTypes";
import {MaterialTypes} from "../../model/MaterialTypes";
import TypeField from "../TypeField";
import CodeField from "../CodeField";

import Component = React.Component

import Cell = ModelTypes.Cell
import Door = ModelTypes.Door
import Split = ModelTypes.Split
import SplitType = ModelTypes.SplitType;

import Type = MaterialTypes.Type
import Code = MaterialTypes.Code
import Fill = ModelTypes.Fill;

interface Props {
    index: number,
    types: Type[],
    selected: boolean,
    fill: Fill,
    onChange: (fill: Fill) => void,
    onSelect: (select: boolean) => void
    splitBar: JSX.Element
}

class FillView extends Component<Props, any> {
    typeField: TypeField;
    codeField: CodeField;

    onTypeChange = (typeId: string): void => {
        const fill: Fill = Object.assign({}, this.props.fill, {typeId: typeId});
        this.props.onChange(fill);
        this.codeField.focus();
    };

    onCodeChange = (codeId: string): void => {
        const fill: Fill = Object.assign({}, this.props.fill, {codeId: codeId});
        this.props.onChange(fill);
    };

    render() {
        const {index, types, fill, selected, splitBar} = this.props;
        const codes: Code[] = Lodash.isNil(fill) || Lodash.isNil(fill.typeId) ? [] :
            types.find((t) => t.id == fill.typeId).codes;

        const onClick = (): void => {
            this.props.onSelect(!this.props.selected);
        };

        const style = {
            cursor: 'pointer'
        };

        return (
            <div className="row">
                <div className="f-form-control">
                    <Avatar style={style} onClick={onClick} backgroundColor={selected ? blue500 : blue50 }
                            color={selected ? blue50 : blue500}>{index + 1}</Avatar>
                </div>
                <div className="f-form-control">
                    <TypeField
                        ref={(input) => this.typeField = input}
                        floatingLabelText="Тип материала"
                        hintText="Выбирите тип материала"
                        types={types}
                        typeId={fill.typeId}
                        onChange={this.onTypeChange}
                    />
                </div>
                <div className="f-form-control">
                    <CodeField
                        ref={(input) => this.codeField = input}
                        floatingLabelText="Код материала"
                        hintText="Выбирите код материала"
                        codes={codes}
                        codeId={fill.codeId}
                        onChange={this.onCodeChange}
                    />
                </div>
                {splitBar}
            </div>)
    }
}

export default FillView;
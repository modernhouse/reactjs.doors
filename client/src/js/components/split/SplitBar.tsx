import * as React from "react";
import IconButton from "material-ui/IconButton";
import VerticalIcon from "material-ui/svg-icons/editor/border-vertical";
import HorizontalIcon from "material-ui/svg-icons/editor/border-horizontal";
import {ModelTypes} from "../../model/ModelTypes";


import Component = React.Component;

import Cell = ModelTypes.Cell;
import Door = ModelTypes.Door;
import Split = ModelTypes.Split;
import SplitType = ModelTypes.SplitType;

interface Props {
    disabled: boolean
    addSplit: (type: SplitType) => void
}

class SplitBar extends Component<Props, any> {
    render() {
        const {disabled, addSplit} = this.props;

        const onClickHorizontal = (): void => {
            addSplit(SplitType.horizontal);
        };
        const onClickVertical = (): void => {
            addSplit(SplitType.vertical)
        };

        return (
            <span>
                <div className="f-form-control">
                    <IconButton disabled={disabled}
                                tooltip="Горизонтальный разделитель"
                                onClick={onClickHorizontal}><HorizontalIcon/></IconButton>
                </div>
                <div className="f-form-control">
                    <IconButton disabled={disabled}
                                tooltip="Вертикальный разделитель"
                                onClick={onClickVertical}><VerticalIcon/></IconButton>
                </div>
            </span>
        )
    }
}
export default SplitBar
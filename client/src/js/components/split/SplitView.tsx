import * as React from "react";
import ReactCSSTransitionGroup from "react-addons-css-transition-group";
import TextField from "material-ui/TextField";
import IconButton from "material-ui/IconButton";
import RemoveIcon from "material-ui/svg-icons/action/delete";
import ExpandLessIcon from "material-ui/svg-icons/navigation/expand-less";
import ExpandMoreIcon from "material-ui/svg-icons/navigation/expand-more";

import {black, blue500, cyan200, green600, white, yellow600} from "material-ui/styles/colors";
import Avatar from "material-ui/Avatar";

import VerticalIcon from "material-ui/svg-icons/editor/border-vertical";
import HorizontalIcon from "material-ui/svg-icons/editor/border-horizontal";


import {Card, CardActions, CardHeader, CardText} from "material-ui/Card";
import * as Lodash from "lodash";
import {ModelTypes} from "../../model/ModelTypes";
import {MaterialTypes} from "../../model/MaterialTypes";

import Component = React.Component;

import Cell = ModelTypes.Cell;
import Door = ModelTypes.Door;
import Split = ModelTypes.Split;
import SplitType = ModelTypes.SplitType;
import Type = MaterialTypes.Type;
import Code = MaterialTypes.Code;

interface State {
    expended: boolean
}
interface Props {
    onDelete: () => void;
    onUpdate: (any) => void;
    split: Split;
    disabledBar?: boolean
}

class SplitView extends Component<Props, State> {
    fieldOffset: TextField;

    constructor(props: Props) {
        super(props);
        this.state = {
            expended: true
        }
    }

    icon = (split: Split): any => {
        if (Lodash.isNil(split))
            return <span/>;
        switch (split.type) {
            case ModelTypes.SplitType.vertical:
                return <VerticalIcon/>;
            case ModelTypes.SplitType.horizontal:
                return <HorizontalIcon/>;
            default:
                return <span/>
        }
    };

    render() {
        const expended = this.state.expended;
        const {split} = this.props;
        const onDelete = (e): void => {
            e.stopPropagation();
            this.props.onDelete();
        };
        const onChangeOffset = (event: any, newValue: string): void => {
            this.props.onUpdate({offset: Number(newValue)});
        };

        const onExpend = (e): void => this.setState({expended: !this.state.expended});

        return (
            <div>
                { Lodash.isNil(split) &&
                <div className="row">
                </div>
                }
                { !Lodash.isNil(split) &&
                <div className="row">
                    <div className="f-form-control">
                        <Avatar color={black} backgroundColor={split.color} icon={this.icon(split)}/>
                    </div>
                    <div className="f-form-control">
                        <TextField
                            ref={(input) => this.fieldOffset = input}
                            type="number"
                            hintText="Введите отступ от левого края двери в мм"
                            floatingLabelText="Отступ (мм)"
                            floatingLabelFixed={false}
                            value={split.offset}
                            onChange={onChangeOffset}
                        />
                    </div>
                    <div className="f-form-control">
                        <IconButton onClick={onDelete}><RemoveIcon/></IconButton>
                    </div>
                </div>}
            </div>
        );
    }

    renderFills(expended: boolean, split: Split) {

        return (!Lodash.isNil(split) &&
        <ReactCSSTransitionGroup transitionName="example" transitionEnterTimeout={500}
                                 transitionLeaveTimeout={300}>
            <div className="row">
                {expended && <div>HELLO</div>}
            </div>
        </ReactCSSTransitionGroup>)
    }
}

export default SplitView;

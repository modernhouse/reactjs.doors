import * as React from "react";
import * as JQuery from "jquery";
import RaisedButton from "material-ui/RaisedButton";
import Editor3D, {View} from "../components/3d/Editor3D";
import {ModelTypes} from "../model/ModelTypes";
import {CANVAS_HEIGHT, CANVAS_WIDTH} from "AppConfig";

import Component = React.Component;

import Cell = ModelTypes.Cell;
import Door = ModelTypes.Door;
import Split = ModelTypes.Split;
import SplitType = ModelTypes.SplitType;


class Editor3DContainer extends Component<Props, {}> {
    editorDiv: JQuery;
    editor: Editor3D;

    constructor() {
        super();
        this.state = {
            view: View.front
        }
    }


    componentDidMount(): void {
        this.editorDiv = JQuery('#editor-3d');
        this.editor = new Editor3D(document.getElementById("editor-3d-canvas"),
            CANVAS_WIDTH,
            CANVAS_HEIGHT);
        if (this.props.door != null) {
            this.editor.setDoor(this.props.door)
        }
        this.editor.render();
    }

    componentDidUpdate(prevProps: any, prevState: any, prevContext: any): void {
        this.editor.setDoor(this.props.door);
    }

    changeView(view: View): void {
        this.setState({view: view});
        this.editor.setView(view);
    }

    render() {
        const onClickFront = (): void => this.changeView(View.front);
        const onClickTop = (): void => this.changeView(View.top);
        const onClickRight = (): void => this.changeView(View.right);
        return (
            <div>
                <div id="editor-3d" className="card well" onClick={(e): void => {
                    this.editor.resize(this.editorDiv.width(), this.editorDiv.height())
                }}>
                    <canvas id="editor-3d-canvas"/>
                </div>
                <div id="editor-3d-controls">
                    <RaisedButton onClick={onClickFront}>Front</RaisedButton>
                    <RaisedButton onClick={onClickTop}>Top</RaisedButton>
                    <RaisedButton onClick={onClickRight}>Right</RaisedButton>
                </div>
            </div>
        );
    }
}

interface Props {
    door: Door
}

export default Editor3DContainer;


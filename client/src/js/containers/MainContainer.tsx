import * as React from "react";
import * as Lodash from "lodash";
import {connect} from "react-redux";

import {CloseLeft, CloseRight, DrawerAction, OpenLeft, OpenRight} from "actions/drawer/DrawerActions";
import * as StateService from "services/StateService";


import AppBar from "material-ui/AppBar";
import IconButton from "material-ui/IconButton";
import ChevronLeft from "material-ui/svg-icons/navigation/chevron-left";
import ChevronRight from "material-ui/svg-icons/navigation/chevron-right";

import {Button} from "reactstrap";
import DoorEditor from "containers/door/DoorView";
import DoorList from "containers/doors/ListView";
//material-ui
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import Drawer from "material-ui/Drawer";
import {List, ListItem} from "material-ui/List";
import {Paper} from "material-ui/Paper";
import {blue500, green600, yellow600} from "material-ui/styles/colors";
import Editor3DContainer from "./Editor3DContainer";
import {ModelTypes} from "../model/ModelTypes";
import {doorToString} from "services/DoorService";
import {CANVAS_WIDTH} from "../AppConfig";
import {State} from "../services/StateService";
import {MaterialTypes} from "model/MaterialTypes";

import Component = React.Component;
import Cell = ModelTypes.Cell;
import Door = ModelTypes.Door;
import Split = ModelTypes.Split;
import SplitType = ModelTypes.SplitType;
import Type = MaterialTypes.Type;

const Styles = {
    DoorEditor: {
        shiftRight: {'marginLeft': '265px', 'transition': 'margin-left 150ms ease-in'},
        shiftLeft: {'marginLeft': '5px', 'transition': 'margin-left 150ms ease-in'}
    },
    MainBar: {
        shiftRight: {'marginLeft': '215px', 'transition': 'margin-left 150ms ease-in'},
        shiftLeft: {'marginLeft': '0px', 'transition': 'margin-left 150ms ease-in'}
    }

};

interface Props {
    door: Door
    types: Type[];
    updateDrawer: (DrawerAction) => void
    leftOpen: boolean
    rightOpen: boolean
}

class MainContainer extends Component<Props, any> {

    constructor() {
        super();
    }

    componentDidMount(): void {
    }

    render() {
        const {door, updateDrawer, leftOpen, rightOpen, types} = this.props;
        const openRight = () => updateDrawer({type: OpenRight});
        const closeRight = () => updateDrawer({type: CloseRight});
        const openLeft = () => updateDrawer({type: OpenLeft});
        const closeLeft = () => updateDrawer({type: CloseLeft});

        return (
            <MuiThemeProvider>
                <div id="main-container">
                    <AppBar
                        iconElementRight={<IconButton onClick={openRight}><ChevronLeft/></IconButton>}
                        iconElementLeft={<IconButton onClick={openLeft}><ChevronRight/></IconButton>}
                        title={<span style={leftOpen ? Styles.MainBar.shiftRight : Styles.MainBar.shiftLeft}>{Lodash.isNil(door) ? '' : doorToString(door, types)}</span>}
                    />
                    {this.renderDoorEditor(this.props)}
                    <Drawer open={leftOpen}>
                        <AppBar iconElementRight={<IconButton onClick={closeLeft}><ChevronLeft/></IconButton>}/>
                        <DoorList/>
                    </Drawer>
                    <Drawer open={rightOpen} openSecondary={true} width={CANVAS_WIDTH}>
                        <AppBar
                            iconElementLeft={<IconButton
                                onClick={closeRight}><ChevronRight/></IconButton>}/>
                        <Editor3DContainer door={door}/>
                    </Drawer>
                </div>
            </MuiThemeProvider>);
    }

    renderDoorEditor = (props: Props):any => {
        return (<DoorEditor style={props.leftOpen ? Styles.DoorEditor.shiftRight: Styles.DoorEditor.shiftLeft}/>)
    };

}

function mapStateToProps(state: State) {
    return {
        leftOpen: state.drawer.leftOpen,
        rightOpen: state.drawer.rightOpen,
        door: StateService.getSelectedDoor(state),
        types: state.profileTypes
    };
}

function mapDispatchToProps(dispatch) {
    return {
        updateDrawer: (action: DrawerAction): void => dispatch(action)
    };
}


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MainContainer);

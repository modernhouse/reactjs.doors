import * as React from "react";
import * as Lodash from "lodash";
import * as THREE from "three";

import {connect} from "react-redux";
import {Button} from "reactstrap";
import DoorSize from "components/door/DoorSize";
import DoorType from "components/door/DoorType";
import SplitBar from "components/split/SplitBar";
import FillView from "components/split/FillView";
import SplitView from "components/split/SplitView";

import DoorDetails from "components/door/DoorDetails";
import TextField from "material-ui/TextField";
import FlatButton from "material-ui/FlatButton";


import {Card, CardActions, CardHeader, CardText} from "material-ui/Card";
import {ModelTypes} from "model/ModelTypes";
import {MaterialTypes} from "model/MaterialTypes";
import * as Validators from "model/Validators";

import * as DoorService from "services/DoorService";
import * as StateService from "services/StateService";

import * as MaterialActions from "actions/MaterialActions";
import * as SelectionActions from "actions/selection/SelectionActions";
import * as ModelActions from "actions/model/ModelActions";
import SplitActions from "actions/model/SplitActions";

import Component = React.Component;

import Cell = ModelTypes.Cell;
import Door = ModelTypes.Door;
import Split = ModelTypes.Split;
import SplitType = ModelTypes.SplitType;
import Type = MaterialTypes.Type;
import Code = MaterialTypes.Code;
import Fill = ModelTypes.Fill;
import Vector3 = THREE.Vector3;


interface Props {
    door: Door;
    selectedCellIds: string[];

    profileTypes: Type[];
    fillTypes: Type[];

    updateDoor: (changes: any) => void;
    updateCell: (cellId: string, changes: any) => void;
    updateCellSize: (cellId: string, changes: any) => void;

    addSplit: (type: SplitType) => void;
    updateSplit: (Split, any) => void;
    deleteSplit: (string) => void;
    loadProfileTypes: () => void;
    loadFillTypes: () => void;

    selectCell: (id: string) => void;
    deselectCell: (id: string) => void;
    style: any
}

class DoorView extends Component<Props, {}> {
    doorType: DoorType;
    doorSize: DoorSize;
    fieldName: TextField;
    fieldCount: TextField;

    componentDidMount() {
        this.props.loadProfileTypes();
        this.props.loadFillTypes();
    }


    render() {
        const {door, style} = this.props;
        if (Lodash.isNil(door)) {
            return this.renderEmpty(style);
        } else {
            return this.renderDoor();
        }
    }

    private renderEmpty = (style: any): any => {
        return (
            <div id="door-editor" style={style}>
                <Card>
                    <CardHeader
                        title="Without Avatar"
                        subtitle="Subtitle"
                        actAsExpander={true}
                        showExpandableButton={true}
                    />
                    <CardText expandable={true}>
                        <FlatButton label="Action1"/>
                        <FlatButton label="Action2"/>
                    </CardText>
                </Card>
            </div>
        );
    };


    private renderDoor = (): any => {
        const {
            door, profileTypes,
            updateDoor,
            updateCellSize,
            style
        } = this.props;
        const nextFocus = (): void => this.doorSize.focus();
        const cell = Lodash.isNil(door) ? null : door.cells[0];

        return (
            <div id="door-editor" className="card" style={style}>
                {this.renderDoorDetails()}
                <div id="door-type-size">
                    <DoorType ref={(input) => this.doorType = input} door={door}
                              nextFocus={nextFocus} updateDoor={updateDoor} types={profileTypes}/>
                    <DoorSize ref={(view) => this.doorSize = view}
                              cell={cell}
                              onChange={(changes) => updateCellSize(cell.id, changes)}
                    />
                </div>
                {this.renderCells()}
            </div>
        );
    };

    private renderSplit = (split: Split): any => {
        const {deleteSplit, updateSplit} = this.props;
        return (
            <span>
                <SplitView key={split.id} split={split} onDelete={() => deleteSplit(split.id)}
                           onUpdate={(changes) => updateSplit(split.id, changes)}/>
            </span>
        )
    };

    private renderSplits() {
        const {door} = this.props;
        return door.splits.map(this.renderSplit);
    }


    private renderCell0 = (cell: Cell, index: number): any => {
        const {selectedCellIds, fillTypes, updateCell, selectCell, deselectCell, addSplit} = this.props;
        return (<FillView index={index}
                          fill={cell.fill}
                          types={fillTypes}
                          selected={selectedCellIds.indexOf(cell.id) > -1}
                          onChange={(fill: Fill) => updateCell(cell.id, {fill: fill})}
                          onSelect={(select: boolean) => select ? selectCell(cell.id) : deselectCell(cell.id)}
                          splitBar={<SplitBar addSplit={addSplit}
                                              disabled={!Validators.validateCell(cell) || selectedCellIds.indexOf(cell.id) == -1}/>}
        />)
    };

    private renderCells() {
        const {door} = this.props;
        const doorService: DoorService.DoorService = new DoorService.DoorService(door);

        let vCells: Cell[] = doorService.getVisibleCells();
        let cells: Cell[] = door.cells;
        return vCells.map((c: Cell, index:number) => {
            return this.renderCell0(c, index)
        });
    }

    private renderDoorDetails = () => {
        const {door, updateDoor} = this.props;
        return (<DoorDetails door={door} updateDoor={updateDoor}/>)
    }
}


function mapStateToProps(state, ownProps) {
    return {
        selectedCellIds: state.selection.cellIds,
        door: StateService.getSelectedDoor(state),
        profileTypes: state.profileTypes,
        fillTypes: state.fillTypes,
        style: ownProps.style
    };
}

function mapDispatchToProps(dispatch) {
    return {
        updateDoor: (changes: any): void => dispatch(ModelActions._UpdateDoor(changes)),
        updateCell: (cellId: string, changes: any): void => dispatch(ModelActions._UpdateCell(cellId, changes)),
        updateCellSize: (cellId: string, changes: any): void => dispatch(ModelActions._UpdateCellSize(cellId, changes)),
        addSplit: (type: SplitType): void => dispatch(SplitActions._Add(type)),
        updateSplit: (id: string, changes: any): void => dispatch(SplitActions._Update(id, changes)),
        deleteSplit: (splitId: string): void => {
            dispatch(SplitActions._Delete(splitId))
        },


        loadProfileTypes: (): void => dispatch({type: MaterialActions.ProfileTypesLoad}),
        loadFillTypes: (): void => dispatch({type: MaterialActions.FillTypesLoad}),

        selectCell: (id: string): void => dispatch(SelectionActions._SelectCell(id)),
        deselectCell: (id: string): void => dispatch(SelectionActions._DeselectCell(id))
    };
}


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DoorView);

import * as React from "react";
import {connect} from "react-redux";
//material-ui
import Divider from "material-ui/Divider";
import {List, ListItem, makeSelectable} from "material-ui/List";
import Avatar from "material-ui/Avatar";
import AddDoorIcon from "material-ui/svg-icons/content/add";
import {blue500, green600, yellow600} from "material-ui/styles/colors";

import {ModelTypes} from "model/ModelTypes";
import DoorListItem from "components/DoorListItem";
import * as ModelActions from "actions/model/ModelActions";
import * as SelectionActions from "actions/selection/SelectionActions";

import * as ModelService from "services/ModelService";
import {State} from "services/StateService";

import Component = React.Component;

import Cell = ModelTypes.Cell;
import Split = ModelTypes.Split;
import SplitType = ModelTypes.SplitType;
import Door = ModelTypes.Door;

interface Props {
    addDoor: () => void;
    deleteDoor: (Door) => void;
    selectDoor: (string) => void;
    doors: Door[];
    door: Door;
}


class ListView extends Component<Props, {}> {
    render() {
        const {door, doors, addDoor, deleteDoor, selectDoor} = this.props;
        const onSelect = (d: Door): void => {
            selectDoor(d.id);
        };
        return (
            <List>
                <ListItem leftAvatar={<Avatar icon={<AddDoorIcon />} backgroundColor={green600}/>}
                          primaryText="Добавить дверь"
                          onClick={addDoor}
                />
                <Divider inset={true}/>
                {
                    doors.map((d: Door, index: number) => {
                        return (<DoorListItem door={d} key={index} onDelete={ deleteDoor }
                                              onSelect={ onSelect }
                                              selected={ d == door }/>);
                    })
                }
            </List>
        )
    }
}

function mapStateToProps(state: State) {
    return {
        door: ModelService.getDoor(state.model, state.selection.doorId),
        doors: state.model.doors
    };
}

function mapDispatchToProps(dispatch) {
    return {
        addDoor: (): void => dispatch(ModelActions._AddDoor()),
        deleteDoor: (door: Door): void => dispatch(ModelActions._DeleteDoor(door.id)),
        selectDoor: (id: string): void => dispatch(SelectionActions._SelectDoor(id))
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ListView);


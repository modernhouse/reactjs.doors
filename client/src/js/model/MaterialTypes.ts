export namespace MaterialTypes {
    export type Type = {
        id: string
        name: string
        codes: Code[]
    }

    export type Code = {
        id: string
        code: string
        name: string
        producer?: Producer
    }

    export type Producer = {
        id: string
        name: string
    }
}

import * as THREE from "three"

import {MaterialTypes} from "model/MaterialTypes";
import Type = MaterialTypes.Type;
import Vector3 = THREE.Vector3;
import Cell = ModelTypes.Cell;

export namespace ModelTypes {

    import Code = MaterialTypes.Code;
    export type Model = {
        doors: Door[]
    }

    export type Selection = {
        doorId: string,
        cellIds: string[],
    }

    export type Door = {
        id: string
        name: string
        count: number
        type?: string
        profile: Profile
        splits: Split[]
        cells: Cell[]
        line?: Line
    }

    export type Line = {
        length: number
    }

    export type Cell = {
        id: string
        fill: Fill
        splitId?: string
        size: Vector3
        pos: Vector3
    }

    export type Profile = {
        typeId?: string
        codeId?: string
    }

    export type Split = {
        id?: string
        offset: number
        type: SplitType
        cellIds?: string[]
        color?: string
        size: Vector3
        pos: Vector3
    }

    export type Fill = {
        typeId: string
        codeId: string
    }

    export enum SplitType {
        horizontal,
        vertical
    }
}

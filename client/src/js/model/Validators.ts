import * as Lodash from "lodash";
import * as THREE from "three";

import {ModelTypes} from "./ModelTypes";
import Cell = ModelTypes.Cell;
import Door = ModelTypes.Door;
import Split = ModelTypes.Split;
import SplitType = ModelTypes.SplitType;
import Vector3 = THREE.Vector3;


export const validateDoor = (door: Door): boolean => {
    return (!Lodash.isNil(door) &&  validateCell(door.cells[0]));
};

export const validateCell = (cell: Cell): boolean => {
    return (!Lodash.isNil(cell) && validateSize(cell.size));
};

export const validateSize = (size: Vector3): boolean => {
    return (!Lodash.isNil(size) && size.x > 0 && size.y > 0 && size.z > 0);
};

export const validateSplit = (split: Split, size: Vector3): boolean => {
    if (Lodash.isNil(split))
        return false;

    switch (split.type) {
        case SplitType.vertical:
            return split.offset > 0 && split.offset < size.x;
        case SplitType.horizontal:
            return split.offset > 0 && split.offset < size.y;
        default:
            throw new Error(`${split.type} is not supported`)
    }
};
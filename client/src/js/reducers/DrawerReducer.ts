import {CloseLeft, CloseRight, OpenLeft, OpenRight} from "actions/drawer/DrawerActions";
export type DrawerState = {
    leftOpen: boolean
    rightOpen: boolean
}

export const DrawerReducer = (state: DrawerState = {leftOpen: true, rightOpen: true}, action) => {
    switch (action.type) {
        case OpenLeft:
            return Object.assign({}, state, {leftOpen: true});
        case OpenRight:
            return Object.assign({}, state, {rightOpen: true});
        case CloseLeft:
            return Object.assign({}, state, {leftOpen: false});
        case  CloseRight:
            return Object.assign({}, state, {rightOpen: false});
        default:
            return state;
    }
};
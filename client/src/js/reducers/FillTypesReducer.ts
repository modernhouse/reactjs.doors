import {MaterialTypes} from "../model/MaterialTypes";
import {FillTypesLoaded} from "../actions/MaterialActions";
import {ModelTypes} from "../model/ModelTypes";

import Type = MaterialTypes.Type;
import Model = ModelTypes.Model;

const FillTypesReducer = (state: Type[] = [], action) => {
    switch (action.type) {
        case FillTypesLoaded:
            return action.types;
        default:
            return state
    }
};


export default FillTypesReducer;
import * as Lodash from "lodash";

import * as ModelActions from "actions/model/ModelActions";
import SplitActions from "actions/model/SplitActions";

import {ModelTypes} from "model/ModelTypes";

import DoorService, {getCell, getDoor} from "services/DoorService";
import * as ModelService from "services/ModelService";

import Cell = ModelTypes.Cell;
import Model = ModelTypes.Model;
import Door = ModelTypes.Door;


const ModelReducer = (model: Model = {doors: []}, action) => {
    switch (action.type) {
        case ModelActions.AddDoor:
            return addDoor(model, action);
        case ModelActions.DeleteDoor:
            return deleteDoor(model, action);
        case ModelActions.UpdateDoor:
            return updateDoor(model, action);
        case ModelActions.UpdateCell:
            return updateCell(model, action);
        case SplitActions.Add:
            return addSplit(model, action);
        case SplitActions.Delete:
            return deleteSplit(model, action);
        case SplitActions.Update:
            return updateSplit(model, action);
        default:
            return model;
    }
};

const updateSplit = (model: Model, action: SplitActions.ActionUpdate): Model => {
    let nm: Model = Lodash.cloneDeep(model);
    let door = getDoor(nm.doors, action.doorId);
    let ds: DoorService = new DoorService(door);
    ds.updateSplit(action.split);
    return nm;
};

const deleteSplit = (model: Model, action: SplitActions.ActionDelete): Model => {
    let nm: Model = Lodash.cloneDeep(model);
    let door = getDoor(nm.doors, action.doorId);
    new DoorService(door).deleteSplitById(action.splitId);
    return nm;
};


const addSplit = (model: Model, action: SplitActions.ActionUpdate): Model => {
    let nm: Model = Lodash.cloneDeep(model);
    let door = getDoor(nm.doors, action.doorId);
    let cell = getCell(door, action.cellId);
    cell.splitId = action.split.id;
    new DoorService(door).addSplit(action.split).addCells(action.cells);
    return nm;

};

const updateCell = (model: Model, action: ModelActions.ActionUpdateCell): Model => {
    let nm: Model = Lodash.cloneDeep(model);
    let door: Door = getDoor(nm.doors, action.doorId);
    new DoorService(door).updateCell(action.cell);
    return nm;
};

const deleteDoor = (model: Model, action): Model => {
    let nm: Model = Lodash.cloneDeep(model);
    ModelService.deleteDoor(nm, action.id);
    return nm;
};

const updateDoor = (model: Model, action: ModelActions.ActionUpdateDoor): Model => {
    let nm: Model = Lodash.cloneDeep(model);
    let door = getDoor(nm.doors, action.doorId);
    let idx = nm.doors.indexOf(door);
    nm.doors[idx] = action.door;
    return nm;
};

const addDoor = (model: Model, action): Model => {
    let nm: Model = Lodash.cloneDeep(model);
    nm.doors = [...nm.doors, action.door];
    return nm;
};

export default ModelReducer;
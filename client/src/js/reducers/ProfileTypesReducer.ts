import {MaterialTypes} from "../model/MaterialTypes";
import {ProfileTypesLoad, ProfileTypesLoaded} from "../actions/MaterialActions";
import {ModelTypes} from "../model/ModelTypes";

import Type = MaterialTypes.Type;
import Model = ModelTypes.Model;

const ProfileTypesReducer = (state: Type[] = [], action) => {
    switch (action.type) {
        case ProfileTypesLoad:
            return state;
        case ProfileTypesLoaded:
            return action.types;
        default:
            return state
    }
};


export default ProfileTypesReducer;
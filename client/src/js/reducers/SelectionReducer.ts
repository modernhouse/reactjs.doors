import {ModelTypes} from "model/ModelTypes";
import * as Lodash from "lodash";
import * as SelectionActions from "actions/selection/SelectionActions";
import {List} from "immutable";
import Selection = ModelTypes.Selection

const SelectionReducer = (selection: Selection = {doorId: null, cellIds: []}, action) => {

    let ns: Selection = Lodash.cloneDeep(selection);

    switch (action.type) {
        case SelectionActions.SelectDoor:
            ns.doorId = action.doorId;
            return ns;
        case SelectionActions.DeselectDoor:
            ns.doorId = null;
            ns.cellIds = [];
            return ns;
        case SelectionActions.SelectCell:
            ns.cellIds = List(ns.cellIds).concat(action.id).toArray();
            return ns;
        case SelectionActions.DeselectCell:
            ns.cellIds = List(ns.cellIds).filter((id) => id != action.id).toArray();
            return ns;
        case SelectionActions.DeselectAllCells:
            ns.cellIds = [];
            return ns;
        default:
            return selection;
    }
};

export default SelectionReducer;
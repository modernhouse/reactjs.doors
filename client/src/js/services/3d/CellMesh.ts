import * as THREE from "three";
import FONT from "three/examples/fonts/droid/droid_serif_regular.typeface.json";

import Style from "services/3d/Style";
import {ModelTypes} from "model/ModelTypes";

import Vector3 = THREE.Vector3;
import Mesh = THREE.Mesh;
import BoxGeometry = THREE.BoxGeometry;
import MeshNormalMaterial = THREE.MeshNormalMaterial;
import MeshBasicMaterial = THREE.MeshBasicMaterial;
import Group = THREE.Group;
import Material = THREE.Material;
import TextGeometry = THREE.TextGeometry;
import FontLoader = THREE.FontLoader;
import Font = THREE.Font;
import Cell = ModelTypes.Cell;
import Split = ModelTypes.Split;

const CircleSegments: number = 64;

class CellMesh extends Group {
    index: number;
    cell: Cell;
    geometry: BoxGeometry;
    mesh: Mesh;
    material: Material;
    label: Mesh;
    text: Mesh;

    constructor(cell: Cell, index: number, material: Material) {
        super();
        this.index = index;
        this.cell = cell;
        this.material = material;
        this.geometry = new BoxGeometry(cell.size.x, cell.size.y, cell.size.z);
        this.mesh = new Mesh(this.geometry, this.material);
        this.add(this.mesh);
        this.label = this.createLabel();
        this.add(this.label);
        this.text = this.createText();
        this.add(this.text);
        this.position.set(cell.pos.x, cell.pos.y, cell.pos.z);
    }

    private createLabel(): Mesh {
        let material: THREE.LineBasicMaterial = new THREE.LineBasicMaterial({color: Style.labelBackgroundColor});
        material.transparent = true;
        material.opacity = Style.labelOpacity;
        let geometry: THREE.CircleGeometry = new THREE.CircleGeometry(this.cell.size.z, CircleSegments);
        let label = new THREE.Mesh(geometry, material);
        label.translateZ(this.cell.size.z / 2);
        return label;
    }

    private createText(): Mesh {
        let loader = new FontLoader();
        let font: Font = loader.parse(FONT);
        let geom = new TextGeometry(`${this.index}`, {size: this.cell.size.z, font: font, height: 1});
        let mat = new THREE.LineBasicMaterial({color: Style.labelColor});
        mat.transparent = true;
        mat.opacity = Style.labelOpacity;
        let mesh = new Mesh(geom, mat);
        mesh.translateX(-this.cell.size.z / 3);
        mesh.translateY(-this.cell.size.z / 2);
        mesh.translateZ(this.cell.size.z / 2);
        return mesh;
    }
}

export default CellMesh
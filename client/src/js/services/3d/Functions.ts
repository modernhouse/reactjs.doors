import * as THREE from "three";


import Style from "services/3d/Style";
import {ModelTypes} from "model/ModelTypes";
import CellMesh from "services/3d/CellMesh";
import MeshBasicMaterial = THREE.MeshBasicMaterial;
import Cell = ModelTypes.Cell;
import Vector3 = THREE.Vector3;
import Split = ModelTypes.Split;
import Vector = THREE.Vector;
import SplitType = ModelTypes.SplitType;


export const SPLIT_WIDTH: number = 50;


export type HResult = {
    top: Vector3,
    split: Vector3,
    bottom: Vector3
}


export type VResult = {
    left: Vector3,
    split: Vector3,
    right: Vector3
}


export const createMaterial = () => {
    return new MeshBasicMaterial({color: Style.cellColor, wireframe: false});
};

export const createCellMech = (cell: Cell, idx: number): CellMesh => {
    return new CellMesh(cell, idx, createMaterial());
};


/**
 * let Wl = W * OFF / W;
 * let Wr = W * (1 - OFF / W);
 */

export const vSplitSize = (site: Vector3, offset: number): VResult => {

    let result: VResult = {
        left: site.clone(),
        right: site.clone(),
        split: site.clone()
    };

    result.left = result.left.multiply(new Vector3(offset / site.x, 1, 1));
    result.right = result.right.multiply(new Vector3(1 - offset / site.x, 1, 1));
    result.split.x = SPLIT_WIDTH / 2;
    return result;
};

/**
 * let Xrp = Xp + Wl / 2;
 * let Xlp = Xp - Wr / 2;
 * let Xsp = Xp + (Wl - Wr)/2;
 */

export const vSplitPos = (size: VResult, pos: Vector3): VResult => {

    let result: VResult = {
        left: pos.clone(),
        right: pos.clone(),
        split: pos.clone()
    };

    result.left.add(new Vector3(-size.right.x / 2, 0, 0));
    result.right.add(new Vector3(size.left.x / 2, 0, 0));
    result.split.add(new Vector3((size.left.x - size.right.x) / 2, 0, 0));
    return result;
};


/**
 * let Ht = H * OFF / H;
 * let Hb = H * (1 - OFF / H);
 */
export const hSplitSize = (cellSize: Vector3, offset: number): HResult => {

    let result: HResult = {
        top: cellSize.clone(),
        bottom: cellSize.clone(),
        split: cellSize.clone()
    };
    result.top = result.top.multiply(new Vector3(1, offset / cellSize.y, 1));
    result.bottom = result.bottom.multiply(new Vector3(1, 1 - offset / cellSize.y, 1));
    result.split.y = SPLIT_WIDTH / 2;

    return result;
};

/**
 * let Ytp = Yp + Hb / 2;
 * let Ybp = Yp - Ht / 2;
 * let Ysp = Yp + (Hb - Ht)/2;
 */
export const hSplitPos = (size: HResult, pos: Vector3): HResult => {
    let result: HResult = {
        top: pos.clone(),
        bottom: pos.clone(),
        split: pos.clone()
    };
    result.top.add(new Vector3(0, size.bottom.y / 2, 0));
    result.bottom.add(new Vector3(0, -size.top.y / 2, 0));
    result.split.add(new Vector3(0, (size.bottom.y - size.top.y) / 2, 0));
    return result;
};




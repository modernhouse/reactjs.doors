import * as THREE from "three";
import Style from "services/3d/Style";
import {ModelTypes} from "model/ModelTypes";

import Group = THREE.Group;
import Vector3 = THREE.Vector3;
import BoxGeometry = THREE.BoxGeometry;
import MeshBasicMaterial = THREE.MeshBasicMaterial;

import Mesh = THREE.Mesh;
import Split = ModelTypes.Split;

export class SplitMesh extends Group {
    split: Split;
    geometry: BoxGeometry;
    material: MeshBasicMaterial;
    mesh: Mesh;

    constructor(split: Split) {
        super();
        this.split = split;
        this.geometry = new BoxGeometry(this.split.size.x, this.split.size.y, this.split.size.z);
        this.material = new MeshBasicMaterial({
            color: split.color,
            wireframe: false
        });
        this.mesh = new Mesh(this.geometry, this.material);
        this.add(this.mesh);
        this.position.set(this.split.pos.x, this.split.pos.y, this.split.pos.z);
    }
}

export default SplitMesh
class Style {
    constructor(doorColor: number = 0xAA4139,
                labelColor: number = 0x72120A,
                labelBackgroundColor: number = 0xFFD6D3,
                splitColor: number = 0xAA6839,
                labelOpacity: number = 1) {
        this.cellColor = doorColor;
        this.labelColor = labelColor;
        this.labelBackgroundColor = labelBackgroundColor;
        this.splitColor = splitColor;
        this.labelOpacity = labelOpacity;
    }

    cellColor: number;
    labelColor: number;
    labelBackgroundColor: number;
    splitColor: number;
    labelOpacity: number;
}

const defaultStyle = new Style();

export default defaultStyle;
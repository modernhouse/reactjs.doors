import * as THREE from "three";
import randomcolor from "randomcolor";

import {ModelTypes} from "../model/ModelTypes";
import * as Lodash from "lodash";
import uuid from "uuid";
import {MaterialTypes} from "../model/MaterialTypes";

import Cell = ModelTypes.Cell
import Door = ModelTypes.Door
import Split = ModelTypes.Split
import Vector3 = THREE.Vector3;
import SplitType = ModelTypes.SplitType;
import Fill = ModelTypes.Fill;
import Type = MaterialTypes.Type;
import Profile = ModelTypes.Profile;


export class DoorService {
    private door: Door;

    constructor(door: Door) {
        this.door = door;
    }

    addCell = (cell: Cell): DoorService => {
        addCell(this.door, cell);
        return this;
    };

    addCells = (cells: Cell[]): DoorService => {
        this.door.cells = Lodash.concat(this.door.cells, cells);
        return this;
    };

    updateCell = (cell: Cell): DoorService => {
        let old: Cell = this.getCell(cell.id);
        let idx: number = this.door.cells.indexOf(old);
        this.door.cells[idx] = cell;
        return this;
    };

    updateSplit = (split: Split): DoorService => {
        let old: Split = this.getSplit(split.id);
        let idx: number = this.door.splits.indexOf(old);
        this.door.splits[idx] = split;
        return this;
    };


    addSplit = (split: Split): DoorService => {
        this.door.splits = Lodash.concat(this.door.splits, split);
        return this;
    };

    deleteSplit = (split: Split): DoorService => {
        this.deleteSplitById(split.id);
        return this;
    };

    deleteSplitByIdx = (idx: number): DoorService => {
        let split: Split = this.door.splits[idx];
        this.deleteSplit(split);
        return this;
    };

    deleteSplitById = (id: string): DoorService => {
        let split: Split = Lodash.find(this.door.splits, (s) => s.id == id);

        Lodash.remove(this.door.splits, (s) => s.id == id);

        Lodash.remove(this.door.cells, (c) => split.cellIds.indexOf(c.id) > -1);

        let cell: Cell = getCellBySplitId(this.door, id);
        if (!Lodash.isNil(cell)) {
            cell.splitId = null;
        }
        return this;
    };

    getCell = (id: string): Cell => {
        return getCell(this.door, id);
    };

    getSplit = (id: string): Split => {
        return getSplit(this.door, id);
    };

    getVisibleCells = (): Cell[] => {
        return this.door.cells.filter((c) => Lodash.isNil(c.splitId))
            .sort((c1, c2) => {

                let x1:number = c1.pos.x - c1.size.x/2;
                let y1:number = c1.pos.y + c1.size.y/2;

                let x2:number = c2.pos.x - c2.size.x/2;
                let y2:number = c2.pos.y + c2.size.y/2;

                let r:number = y2 - y1;

                if (r == 0) {
                    r = x1 - x2;
                }
                return r;
            });
    }
}

export const doorToString = (door: Door, types: Type[]): string => {
    let result: string = '';
    result += `${door.name}:`;
    result += Lodash.isNil(door.type) ? '' : `${door.type}, `;
    result += profileToString(door.profile, types);
    result += `${door.count} шт.`;
    return result;
};

export const profileToString = (profile: Profile, types: Type[]): string => {
    let result: string = '';
    if (!(Lodash.isNil(profile) || Lodash.isNil(profile.codeId) || Lodash.isNil(profile.typeId))) {
        let type = types.find((t) => t.id == profile.typeId);
        let code = type.codes.find((c) => c.id == profile.codeId);
        result = `${type.name}-${code.name}, `
    }
    return result;
};


export const getSplit = (door: Door, id: string): Split => {
    return Lodash.find(door.splits, (s) => s.id == id);
};

export const getCellBySplitId = (door: Door, splitId: string): Cell => {
    return Lodash.find(door.cells, (c) => c.splitId == splitId);
};

export const getSplitByCellId = (door: Door, cellId: string): Split => {
    return Lodash.find(door.splits, (s) => s.cellIds.indexOf(cellId) > -1);
};

export const getCell = (door: Door, id: string): Cell => {
    return Lodash.find(door.cells, (c) => c.id == id);
};

export const getDoor = (doors: Door[], id: string): Door => {
    return doors.find((d) => d.id == id);
}

export const createDoor = (index: number): Door => {
    let door: Door = {
        id: uuid(),
        name: `Дверь-${index}`,
        count: 1,
        profile: {},
        splits: [],
        cells: []
    };
    addCell(door, createCell());
    return door
};

export const createCell = (fill: Fill = {typeId: null, codeId: null}): Cell => {
    return {
        id: uuid(),
        size: new Vector3(1000, 2000, 50),
        fill: Lodash.clone(fill),
        pos: new Vector3(0, 0, 0)
    };
};

export const createSplit = (cell: Cell, type: SplitType, cells: Cell[]): Split => {
    let color: string = randomcolor({
        luminosity: 'light',
        hue: 'blue',
        format: 'hex'
    });
    let split: Split = {
        id: uuid(),
        type: type,
        cellIds: [cells[0].id, cells[1].id],
        offset: 0,
        size: new Vector3(),
        pos: new Vector3(),
        color: color
    };


    switch (type) {
        case SplitType.vertical:
            split.offset = Math.round(cell.size.x / 2);
            break;
        case SplitType.horizontal:
            split.offset = Math.round(cell.size.y / 2);
            break;
        default:
            throw new Error()
    }
    return split;
};

const addCell = (door: Door, cell: Cell): Door => {
    door.cells = Lodash.concat(door.cells, cell);
    return door;
};


export default DoorService;
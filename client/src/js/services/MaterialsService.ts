import * as Lodash from "lodash"
import {MaterialTypes} from "../model/MaterialTypes";
import Type = MaterialTypes.Type;
import Code = MaterialTypes.Code;
import {ModelTypes} from "../model/ModelTypes";
import Door = ModelTypes.Door;




export  const formatCode = (code: Code): string => {
    return Lodash.isNil(code) ? '' : `${code.code}:${code.name}`
};

export const formatType = (type: Type): string => {
    return Lodash.isNil(type) ? '' : type.name;
};

export const loadProfileTypes = (): Type[] => {
    let result: Type[] = [];

    let senatorCodes: Code[] = [{id: '17', name: "Серебро", code: 'SO01'},
        {id: '18', name: "Золото", code: 'SO01'},
        {id: '19', name: "Шампань", code: 'SO02'},
        {id: '20', name: "Коньяк", code: 'SO03'}
    ];


    result = result.concat(
        {
            id: "1",
            name: 'MODUS',
            codes: [{id: '1', name: "Серебро", code: 'A00'},
                {id: '2', name: "Серебро глянец", code: 'A02'},
                {id: '3', name: "Шампань глянец", code: 'A05'},
                {id: '4', name: "Шампань", code: 'A06'},
                {id: '5', name: "Коньяк", code: 'A07'},
                {id: '6', name: "Черный матовый", code: 'A15'},
                {id: '7', name: "Белый", code: 'A16'},
                {id: '8', name: "Золото", code: 'A20'},
                {id: '9', name: "Золото розовое", code: 'A22'},
                {id: '10', name: "Золото розовое глянец", code: 'A23'},
                {id: '11', name: "Серебро шпифованное", code: 'A29'},
                {id: '12', name: "Клен", code: 'A30'},
                {id: '13', name: "Венге", code: 'A31'},
                {id: '14', name: "Красное дерево", code: 'A32'},
                {id: '15', name: "Вишня", code: 'A33'},
                {id: '16', name: "Орех темный", code: 'A34'}]
        },
        {
            id: "2",
            name: 'SENATOR-ОТКРЫТЫЙ',
            codes: senatorCodes
        },
        {
            id: "3",
            name: 'SENATOR-ЗАКРЫТЫЙ',
            codes: senatorCodes
        },
        {
            id: "4",
            name: 'SENATOR-ЭЛИТ',
            codes: senatorCodes
        },
        {
            id: "5",
            name: 'SENATOR-ПРЕСТИЖ',
            codes: senatorCodes
        },
    );
    return result;
};

export const loadFillTypes = (): Type[] => {
    let result: Type[] = [];

    result = result.concat(
        [
            {
                id: "1",
                name: 'ДСП',
                codes: [
                    {id: '0', name: 'Антрацитовый 18', code: 'U164', producer: {id: '0', name: 'Swiss Krono'}},
                    {id: '1', name: 'Белая Аляска', code: 'U8681', producer: {id: '0', name: 'Swiss Krono'}},
                    {id: '2', name: 'Ясень Милан', code: 'D1917', producer: {id: '0', name: 'Swiss Krono'}},
                    {id: '3', name: 'Ясень Москва', code: 'D1926', producer: {id: '0', name: 'Swiss Krono'}},
                    {id: '4', name: 'Бук Цюрих', code: 'D3807', producer: {id: '0', name: 'Swiss Krono'}},
                    {id: '5', name: 'Бук Вена', code: 'D3809', producer: {id: '0', name: 'Swiss Krono'}},
                    {id: '6', name: 'Орех Венеция', code: 'D3811', producer: {id: '0', name: 'Swiss Krono'}},
                    {id: '7', name: 'Орех Барселона', code: 'D3813', producer: {id: '0', name: 'Swiss Krono'}},
                    {id: '8', name: 'Стиль Стамбул', code: 'D3827', producer: {id: '0', name: 'Swiss Krono'}},
                    {id: '9', name: '(VL) Стиль', code: 'D3828', producer: {id: '0', name: 'Swiss Krono'}},
                    {id: '10', name: '(VL) Светло-бежевый', code: 'U119', producer: {id: '0', name: 'Swiss Krono'}},
                    {id: '11', name: '(VL) Ванильный', code: 'U3261', producer: {id: '0', name: 'Swiss Krono'}},
                    {id: '12', name: '(VL) Морская', code: 'U3824', producer: {id: '0', name: 'Swiss Krono'}},
                    {id: '13', name: '(VL) Бирюзовый', code: 'U159', producer: {id: '0', name: 'Swiss Krono'}},
                    {id: '14', name: 'Дуб Осло', code: 'D3315', producer: {id: '0', name: 'Swiss Krono'}},
                    {id: '15', name: 'Дуб Хельсинки', code: 'D3316', producer: {id: '0', name: 'Swiss Krono'}},
                    {id: '16', name: 'Дуб Лондон', code: 'D3798', producer: {id: '0', name: 'Swiss Krono'}},
                    {id: '17', name: 'Дуб Париж', code: 'D3799', producer: {id: '0', name: 'Swiss Krono'}},
                    {id: '18', name: 'Дуб Рим', code: 'D3800', producer: {id: '0', name: 'Swiss Krono'}},
                    {id: '19', name: 'Дуб Мадрид', code: 'D3801', producer: {id: '0', name: 'Swiss Krono'}},
                ],
            },
            {
                id: "2",
                name: 'Стекло',
                codes: [
                    {id: '1', name: 'Стекло прозрачное', code: 'GT'},
                    {id: '2', name: 'Лакомат', code: 'GL'},
                    {id: '3', name: 'Сатин бесцветный', code: 'GST'},
                    {id: '4', name: 'Сатин бронза', code: 'GSB'},
                    {id: '5', name: 'Тонированная бронза', code: 'GTB'},
                    {id: '6', name: 'Тонированный графит', code: 'GTG'},
                    {id: '7', name: 'Пирамида', code: 'GUP'},
                    {id: '8', name: 'Пирамида бронза', code: 'GUPB'},
                    {id: '9', name: 'Сантук', code: 'GUS'},
                    {id: '10', name: 'Сантук бронза', code: 'GUSB'},
                    {id: '11', name: 'Пунто', code: 'GUP'},
                    {id: '12', name: 'Листрал К', code: 'GULK'},
                    {id: '13', name: 'Эстриадо', code: 'GUE'},
                    {id: '14', name: 'Кризет', code: 'GUK'}
                ]
            },
            {
                id: "3",
                name: 'Зеркало',
                codes: [
                    {id: '1', name: 'Серебро', code: 'ZBS'},
                    {id: '2', name: 'Бронза', code: 'ZBB'},
                    {id: '3', name: 'Графит', code: 'ZBG'},
                    {id: '4', name: 'Сатин', code: 'ZSS'},
                    {id: '5', name: 'Сатин бронза', code: 'ZSB'},
                    {id: '6', name: 'Сатин графит', code: 'ZSG'},
                    {id: '7', name: 'Элегант', code: 'ZUE'},
                    {id: '8', name: 'Китайская деревня', code: 'ZUC'}
                ]
            },
            {
                id: "4",
                name: 'Пластик',
                codes: []
            },
            {
                id: "5",
                name: 'Бамбук',
                codes: []
            },
            {
                id: "6",
                name: 'Ротанг',
                codes: []
            },
            {
                id: "7",
                name: 'Эко-кожа',
                codes: []
            },
            {
                id: "8",
                name: 'Витражи',
                codes: []
            }
        ]
    );
    return result;
};



import * as Lodash from "lodash";
import {List} from "immutable";

import {ModelTypes} from "model/ModelTypes";
import Model = ModelTypes.Model;
import Door = ModelTypes.Door;

export const getDoor = (model: Model, id: string): Door => {
    return model.doors.find((d) => d.id == id);
};

export const deleteDoor = (model: Model, id: string): void => {
    let door: Door = getDoor(model, id);
    let index = model.doors.indexOf(door);
    model.doors = [...List(model.doors).remove(index).toArray()];
};

export const lastDoor = (model: Model): Door => {
    return Lodash.last(model.doors);
};
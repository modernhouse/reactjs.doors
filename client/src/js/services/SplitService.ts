import * as THREE from "three";
import * as Lodash from "lodash";


import {ModelTypes} from "model/ModelTypes";
import * as Functions from "services/3d/Functions";

import Cell = ModelTypes.Cell;
import Split = ModelTypes.Split;
import Vector3 = THREE.Vector3;
import SplitType = ModelTypes.SplitType;


export type SplitResult = {
    cells: Cell[],
    split: Split
}

export type SplitRequest = {
    cell: Cell,
    split: Split,
    cells: Cell[]
}

export const processSplit = (request: SplitRequest): SplitResult => {
    switch (request.split.type) {
        case SplitType.vertical:
            return processVSplit(request);
        case SplitType.horizontal:
            return processHSplit(request);
        default:
            throw new Error()
    }
};

export const processHSplit = (request: SplitRequest): SplitResult => {
    let cellSize: Vector3 = request.cell.size.clone();
    let cellPos: Vector3 = request.cell.pos.clone();
    let sizeResult: Functions.HResult = Functions.hSplitSize(cellSize, request.split.offset);
    let posResult: Functions.HResult = Functions.hSplitPos(sizeResult, cellPos);

    let sr: SplitResult = {
        cells: [],
        split: null
    };

    sr.split = Lodash.cloneDeep(request.split);
    sr.split.size = sizeResult.split;
    sr.split.pos = posResult.split;

    let top: Cell = Lodash.cloneDeep(request.cells[0]);
    top.size = sizeResult.top;
    top.pos = posResult.top;

    let bottom: Cell = Lodash.cloneDeep(request.cells[1]);
    bottom.size = sizeResult.bottom;
    bottom.pos = posResult.bottom;

    sr.split.cellIds = [top.id, bottom.id];
    sr.cells = [top, bottom];
    return sr;
};

export const processVSplit = (request: SplitRequest): SplitResult => {
    let size: Vector3 = request.cell.size.clone();
    let pos: Vector3 = request.cell.pos.clone();
    let sizeR: Functions.VResult = Functions.vSplitSize(size, request.split.offset);
    let posR: Functions.VResult = Functions.vSplitPos(sizeR, pos);


    let sr: SplitResult = {
        cells: [],
        split: null
    };

    sr.split = Lodash.cloneDeep(request.split);
    sr.split.size = sizeR.split;
    sr.split.pos = posR.split;

    let left: Cell = Lodash.cloneDeep(request.cells[0]);
    left.size = sizeR.left;
    left.pos = posR.left;

    let right: Cell = Lodash.cloneDeep(request.cells[1]);
    right.size = sizeR.right;
    right.pos = posR.right;

    sr.cells = [left, right];
    return sr;
};

import {ModelTypes} from "model/ModelTypes";
import {MaterialTypes} from "model/MaterialTypes";
import {DrawerState} from "reducers/DrawerReducer";

import * as ModelService from "services/ModelService";

import Door = ModelTypes.Door;
import Model = ModelTypes.Model;
import Type = MaterialTypes.Type;
import Selection = ModelTypes.Selection;


export type State = {
    model: Model
    selection: Selection
    profileTypes: Type[]
    fillTypes: Type[]
    drawer: DrawerState
}

export const getSelectedDoor = (state: State): Door => {
    return ModelService.getDoor(state.model, state.selection.doorId);
};

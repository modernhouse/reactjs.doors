const path = require("path");
const __common = require("./webpack/common");

const config = {
    entry: ["./src/js/app3d.tsx"],
};

module.exports = Object.assign({}, config, __common(__dirname));

const path = require("path");
const {__common, KEYS} = require("./webpack/common");


const __DEFAULT_ENTRY = "./src/js/app.tsx";


module.exports = function (options = {}) {
  options[KEYS.ENTRY] = options[KEYS.ENTRY] || __DEFAULT_ENTRY;
  return Object.assign({}, __common(__dirname, options));
};

const path = require("path");

const KEYS = {
  ENTRY: "entry"
};

const __common = (dirname, options = {}) => {
    return {
      entry: [ options[KEYS.ENTRY] ],
      output: {
            path: path.resolve(__dirname, "build"),
            publicPath: "/assets/",
            filename: "bundle.js"
        },
        resolve: {
            modules: [
                path.resolve(dirname, 'src/js'),
                path.resolve(dirname, 'node_modules')
            ],
            extensions: [".ts", ".tsx", ".js", ".css"]
        },
        module: {
            rules: [
                {
                    test: /\.scss$/,
                    loaders: ['style-loader', 'css-loader', 'sass-loader'],
                    include: [
                        path.resolve(dirname, "src/scss"),
                    ]
                },
                {
                    test: /\.tsx?$/,
                    loader: 'ts-loader',
                    include: [
                        path.resolve(dirname, "src/js"),
                    ],
                },
                {
                    enforce: "pre", test: /\.js$/, loader: "source-map-loader"
                }
            ]
        },
        devServer: {
            inline: false
        },
        devtool: 'source-map'
    }
}

module.exports = {__common, KEYS};

